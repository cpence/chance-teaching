% Fonts
\usepackage[textosf,mathlf,minionint,footnotefigures]{MinionPro}
\usepackage{MyriadPro}

% Titles (for guide)
\usepackage[stable]{footmisc}
\usepackage{titling}
\pretitle{\begin{center}\Large\sffamily\bfseries}
            \posttitle{\par\end{center}\vspace{0.2em}}
\preauthor{\begin{center}\sffamily\bfseries}
            \postauthor{\par\end{center}}
\predate{\begin{center}\sffamily}
            \postdate{\par\rule{0.66\textwidth}{1pt}\end{center}}

% ToC (for guide)
\usepackage{tocloft}
\renewcommand{\cfttoctitlefont}{\sffamily\bfseries\upshape\large}
\renewcommand{\cftsecfont}{\sffamily}
\renewcommand{\cftsecpagefont}{\sffamily}
\setlength\cftbeforesecskip{1pt}

% Headings
\RequirePackage[tiny,sf]{titlesec}
\newcommand{\cpahfamily}{\sffamily}
\newcommand{\cpahbigstyle}{\bfseries\upshape\large}
\newcommand{\cpahmedstyle}{\bfseries\normalsize}
\newcommand{\cpahsmallstyle}{\upshape}

\titleformat{\section}[hang]
{\cpahfamily\cpahbigstyle}
{\thesection.}
{0.5em}
{}
\titlespacing*{\section}{0pt}{\baselineskip}{0.84\baselineskip}
\titleformat{\subsection}[hang]
{\cpahfamily\cpahmedstyle}
{\thesubsection.}
{0.5em}
{}

% Figures and captions
\usepackage[font={footnotesize},labelformat=empty]{caption}
\usepackage{wrapfig}
\setlength{\intextsep}{0pt}

% Framed boxes and footnotes within them
\usepackage{mdframed}
\usepackage{footnote}

% Hyperlinks (color will have been set already)
\usepackage{hyperref}
\hypersetup{
      colorlinks=true,
      linkcolor={linkcolor},
      urlcolor={linkcolor},
      citecolor={black},
      filecolor={black},
      menucolor={black},
      runcolor={black},
      anchorcolor={black}
}

% Because the layout is so odd, allow TeX to emit short pages
\AtBeginDocument{\raggedbottom}

% Tikz graphics for fancy quotation environment
\usepackage{framed}
\usepackage{tikz}
\usetikzlibrary{decorations.pathmorphing,calc}
\pgfmathsetseed{1} % To have predictable results

% Define a background layer, in which the parchment shape is drawn
\pgfdeclarelayer{background}
\pgfsetlayers{background,main}

% define styles for the normal border and the torn border
\tikzset{
      normal border/.style={boxbg, decorate,
                  decoration={random steps, segment length=2.5cm, amplitude=.7mm}},
      torn border/.style={boxbg!50, decorate,
                  decoration={random steps, segment length=.5cm, amplitude=1.7mm}}}

% Macro to draw the shape behind the text, when it fits completely on the page
\def\parchmentframe#1{
      \tikz{
            \node[inner sep=2em] (A) {#1};  % Draw the text of the node
            \begin{pgfonlayer}{background}  % Draw the shape behind
                  \fill[normal border]
                  (A.south east) -- (A.south west) --
                  (A.north west) -- (A.north east) -- cycle;
            \end{pgfonlayer}}}

% Macro to draw the shape, when the text will continue on the next page
\def\parchmentframetop#1{
      \tikz{
            \node[inner sep=2em] (A) {#1};  % Draw the text of the node
            \begin{pgfonlayer}{background}
                  \fill[normal border]  % Draw the ``complete shape'' behind
                  (A.south east) -- (A.south west) --
                  (A.north west) -- (A.north east) -- cycle;
                  \fill[torn border]  % Add the torn lower border
                  ($(A.south east)-(0,.2)$) -- ($(A.south west)-(0,.2)$) --
                  ($(A.south west)+(0,.2)$) -- ($(A.south east)+(0,.2)$) -- cycle;
            \end{pgfonlayer}}}

% Macro to draw the shape, when the text continues from previous page
\def\parchmentframebottom#1{
      \tikz{
            \node[inner sep=2em] (A) {#1};  % Draw the text of the node
            \begin{pgfonlayer}{background}
                  \fill[normal border]   % Draw the ``complete shape'' behind
                  (A.south east) -- (A.south west) --
                  (A.north west) -- (A.north east) -- cycle;
                  \fill[torn border]  % Add the torn upper border
                  ($(A.north east)-(0,.2)$) -- ($(A.north west)-(0,.2)$) --
                  ($(A.north west)+(0,.2)$) -- ($(A.north east)+(0,.2)$) -- cycle;
            \end{pgfonlayer}}}

% Macro to draw the shape, when both the text continues from previous page
% and it will continue on the next page
\def\parchmentframemiddle#1{
      \tikz{
            \node[inner sep=2em] (A) {#1};  % Draw the text of the node
            \begin{pgfonlayer}{background}
                  \fill[normal border]  % Draw the ``complete shape'' behind
                  (A.south east) -- (A.south west) --
                  (A.north west) -- (A.north east) -- cycle;
                  \fill[torn border]   % Add the torn lower border
                  ($(A.south east)-(0,.2)$) -- ($(A.south west)-(0,.2)$) --
                  ($(A.south west)+(0,.2)$) -- ($(A.south east)+(0,.2)$) -- cycle;
                  \fill[torn border]  % Add the torn upper border
                  ($(A.north east)-(0,.2)$) -- ($(A.north west)-(0,.2)$) --
                  ($(A.north west)+(0,.2)$) -- ($(A.north east)+(0,.2)$) -- cycle;
            \end{pgfonlayer}}}

% Define the environment which puts the frame
% In this case, the environment also accepts an argument with an optional
% title (which defaults to ``Example'', which is typeset in a box overlaid
% on the top border
\newenvironment{parchment}[1][Example]{%
      \begin{savenotes}%
            \def\FrameCommand{\parchmentframe}%
            \def\FirstFrameCommand{\parchmentframetop}%
            \def\LastFrameCommand{\parchmentframebottom}%
            \def\MidFrameCommand{\parchmentframemiddle}%
            \vskip\baselineskip
            \MakeFramed {\FrameRestore}
            \noindent\tikz\node[inner sep=1ex, draw=black!20,fill=white,
                  anchor=west, overlay] at (0em, 2em) {\sffamily#1};\par}%
            {\endMakeFramed\end{savenotes}}

\renewenvironment{quote}{\begin{parchment}}{\end{parchment}}

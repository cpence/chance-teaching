# History of Biology Teaching Materials

_[Charles H. Pence,](https://charlespence.net) Université catholique de Louvain_  
_French translations by [Sandra Mouton](http://www.sandramouton.com/)_

This is the source material used to generate the history of biology teaching lessons that form something of a companion to my book, _The Rise of Chance in Evolutionary Theory: A Pompous Parade of Arithmetic._

## Versions

- **1.0, December 20, 2020:** Initial version of lessons and guides in French and English.

## Code Features

The code that builds these lessons might be of interest, if only because it does some pretty fancy stuff in terms of building a single Markdown file into a variety of interesting output formats.

A Pandoc Lua filter (`build/filter.lua`) is building special environments around quotations from primary sources and "box" material for the "THINK" questions found in the text. It's also preparing special-classed images that are flowed in-text in LaTeX using the `wrapfig` package. Images can also have footnotes for attribution, specified as a `footnote` attribute.

The build happens almost exclusively via Pandoc itself; SCons is used to keep track of dependencies. A few stray `sed` commands swap a few things around in the HTML. The `build` directory contains a set of snippets that control paper size, color vs. black-and-white output options, language-specific typesetting options, and a general common bit that contains fonts as well as definitions of environments for the fancy quotes and boxes.

You won't be able to build these exactly yourself unless you have access to Adobe's Minion Pro and Myriad Pro font packages.

## Downloads

To download the printable PDF versions of these lessons, visit DOI [10.5281/zenodo.4704737](https://doi.org/10.5281/zenodo.4704737), or the [UCLouvain OER](https://hdl.handle.net/20.500.12279/802).

## License

- The English-language teaching materials (i.e., the Markdown files named `Vignette.en.md` and `Guide.en.md` in various subdirectories here) are released under [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/).
- The French-language teaching materials (i.e., the Markdown files named `Vignette.fr.md` and `Guide.fr.md` in various subdirectories here) are released under [CC-BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).
- Images (present in the `Images` folder under the subdirectory for each lesson) have individual licensing terms, which are detailed in the image captions in each Markdown file (the majority are public domain or Creative Commons images found on Wikimedia Commons).
- All remaining code/scripts/TeX here are released as public domain under [CC0](https://creativecommons.org/publicdomain/zero/1.0/).

## Acknowledgments

This material was prepared in part with funding from the US National Science Foundation, under HPS Scholars Award #1826784, the Fonds de la Recherche Scientifique --- FNRS, under grant no. F.4526.19, and the « Université numérique » program of the Université catholique de Louvain.

# 3. Lamarck and the Inheritance of Acquired Characters

If we look at change in the natural world, it is hard not to be surprised by some of the peculiar adaptations that we see. To take what might be the most common example, think of the neck of a giraffe. Obviously it's a great advantage for the giraffe to be able to eat leaves from high in the trees. And obviously, too, the giraffe would *like* to be able to get more food. So a perfectly natural way to describe what might have happened to giraffes would be to say that they kept stretching their necks, over and over again, desperately trying to eat the highest leaves, and as a response, their necks grew. We even see cases like this in our own lives: if you lift lots of weights, your muscles grow in response. Why wouldn't evolution be like this?

:::: think
**THINK[1]:** For many scientists over hundreds of years, this conclusion seemed obvious; yet for us now it might seem strange. What do you think about it? More broadly, when is our everyday experience useful as a source of scientific knowledge, and when might it mislead us? How can we tell the difference?
::::

![A giraffe helps itself to some very high food, Kruger National Park, South Africa (CC-BY-SA; by Esculapio at Wikimedia Commons)](giraffe.jpg){.smallimg width=33%}

As it turns out, evolution *isn't* like this (or, at least, it's almost always not like this, though more about that later), but this is one of the oldest and most common misunderstandings about the evolutionary process. Many of the most prominent scientists for more than a century were convinced that it must be the case that, to describe the problem as they would have, *acquired characters are inherited* -- that is, that characters that organisms develop during their lives (like stretched necks or big biceps) will be passed on to their offspring. Darwin himself even argued that what he called "the effects of use and disuse," or the ways in which using a part or disusing another part could affect how characters were passed from parents to offspring, was an important factor in cases like the disappearance of eyes from species that live in caves.

One of the first authors to really develop a view of the organic world like this -- and whose name has been attached to it ever since -- was the French zoologist [Jean-Baptiste de Lamarck](https://en.wikipedia.org/wiki/Jean-Baptiste_Lamarck) (1744--1829). His view of the development of life is sometimes called ["transformism,"](https://en.wikipedia.org/wiki/Transmutation_of_species) to indicate that while he did believe that species changed, he didn't have a theory of "evolution" in the modern sense. On the contrary, he had a picture on which life was constantly being created all around us, starting with very simple microorganisms, and steadily evolving upwards in progress and complexity (recall our first reading from Darwin about concepts of "higher and lower"). So microorganisms, in part because they are striving for better lives, will become jellyfish, which will become worms, and onward through insects, molluscs, and finally vertebrates like us.

:::: think
**THINK[2]:** Lamarck is also interesting because of how he relates scientific pride and national pride. For many years in France, it was thought that Lamarck had largely invented the theory of evolution, and Darwin had in some sense stolen the credit. What's different between Darwin's view and the picture of Lamarck's theory illustrated here? How might the question of inventing scientific theories become a matter of national pride or a dispute between countries?
::::

In his central work on biology, the _Zoological Philosophy_ published in 1809, Lamarck himself described the inheritance of acquired characters in terms of what he called two laws of biology:

:::: {.quote title="Lamarck, \emph{Zoological Philosophy} (1809)"}
*First Law*

In every animal which has not passed the limit of its development, a more frequent and continuous use of any organ gradually strengthens, develops, and enlarges that organ, and gives it a power proportional to the length of time it has been so used; while the permanent disuse of any organ imperceptibly weakens and deteriorates it, and progressively diminishes its functional capacity, until it finally disappears.

*Second Law*

All the acquisitions wrought by nature on individuals, through the influence of the environment in which their race has long been placed, and hence through the influence of the predominant use or permanent disuse of any organ; all these are preserved by reproduction to the new individuals which arise, provided that the acquired modifications are common to both sexes, or at least to the individuals which produce the young.

Here we have two permanent truths, which can only be doubted by those who have never observed or followed the operations of nature...[^lamarck]
::::

[^lamarck]: Lamarck, Jean-Baptiste. 1914 [1809]. [_Zoological Philosophy._](https://doi.org/10.5962/bhl.title.101794) Translated by Hugh Elliot. New York: Hafner Publishing Co., p. 113.

The first law, then, argues that whatever parts an organism uses frequently, as a result of doing the same kinds of actions over and over again, will cause those parts to become larger and stronger. Disuse will cause other parts to shrink or even disappear. The second law says that these changes in parts will be inherited, such that after long enough they would become a permanent part of the species. So the stretched neck of the giraffe would, with enough time, become a permanent part of all giraffes.

While the inheritance of acquired characters wasn't the only mechanism of species change for Lamarck (like Darwin, he also believed that changes in the environment of an organism would be extremely important), it was identified closely enough with Lamarck's theory that the inheritance of acquired characters itself would come to be called [*Lamarckism,*](https://www.sciencedirect.com/topics/agricultural-and-biological-sciences/lamarckism) even today.

Even though, as already mentioned, Darwin himself did think that acquired characters were inherited, in the years after the _Origin of Species_ was published, this idea came under increasing attack. The German biologist [August Weismann](https://en.wikipedia.org/wiki/August_Weismann) (1834--1914) was the first to propose that sex cells, like eggs and sperm -- the germ line -- are [separate from the rest of the cells of the organism](http://embryo.asu.edu/handle/10776/8284) -- the somatic cells -- and that no changes present in the somatic cells could be inherited, as only the germ cells were responsible for producing the next generation of organisms. While this was hotly debated at the time (until future advances in cellular biology made it possible to understand the way in which embryos develop from gametes), it seriously weakened the credibility of the theory of the inheritance of acquired characters among at least some biologists.

But not all biologists were dissuaded (we'll look at some of those in this reading, and some in the next reading). One group, largely American, defended Lamarckism as a way to address what they thought were two crucial flaws in Darwin's system: the randomness of variation and the long amount of time that it took for those variations to accumulate.

Let's consider the two problems they are responding to. First, they argue that there is evidence that variation is in fact not random -- that organisms are, as Lamarck claimed, *responding* to their environments, and hence varying in precisely the direction that was needed to deal with the environmental challenges which they were facing. This kind of variation, Darwin argued, was impossible; natural selection, he claimed, is the only feature in evolution that is directed toward increased fitness. The second worry, about the speed of evolution, was a common one in the years just after the _Origin._ It was thought that the earth was *much* younger than we now believe it to be -- because radioactivity was not yet understood, it was thought that [the earth would have cooled off much more quickly from its initial molten state.](https://www.worldcat.org/title/lord-kelvin-and-the-age-of-the-earth/oclc/892196357) That made biologists worry that there wouldn't have been enough time for natural selection to act in order to produce the kinds of organisms that we see today.

:::: think
**THINK[3]:** We saw that an important part of evaluating evolutionary theory was the calculation of the age of the earth that had been derived in physics (and, later, the discovery of radioactivity). How do you think the knowledge generated in physics might be related to biological claims? When would these be useful? Can you think of circumstances where appeals to other sciences might be unhelpful or problematic?

**THINK[4]:** The idea of "random variation" has always been difficult to interpret in evolutionary theory. By it, Darwin means that variations are not biased in the direction of what the organism "wants," but rather occur without regard to whether they will be helpful or harmful. Natural selection, on the contrary, is not at all random, in its ability to drive populations toward increased fitness.

What kinds of misunderstandings do you think could arise from the description of variation as "random?" What difference is made by the presence of natural selection within the evolutionary process? In general, what might be some differences between scientific theories that describe their results as probabilities versus those, like Newtonian physics, that describe precisely what will happen?
::::

These scientists -- who would soon be known as neo-Lamarckians -- argued that Lamarck's two laws, or something like them, could solve both of these problems. One, [Alpheus Hyatt](https://en.wikipedia.org/wiki/Alpheus_Hyatt) (1838--1902), drew much of his experience from marine invertebrates. He argued that the evolution of cephalopods (like today's squid, octopuses, and cuttlefishes) was the best evidence of the truth of Lamarck's principles. Let's look at his text first, as he wrote in a journal article from 1884:

:::: {.quote title="Hyatt, from the journal \emph{Science} (1884)"}
The *efforts* of the Orthoceratite to adapt itself fully to the requirements of a mixed habitat gave the world the Nautiloidea: the *efforts* of the same type to become completely a littoral crawler developed the Ammonoidea. The successive forms of the Belemnoidea arose in the same way; but here the ground-swimming habitat and complete fitness for that was the object, whereas the Sepioidea represent the highest aims as well as the highest attainments of the Orthoceratites, in their surface-swimming and rapacious forms.[^hyatt1]
::::

[^hyatt1]: Hyatt, Alpheus. 1884. "The Evolution of the Cephalopoda.—I." _Science_ 3 (52): 122--7, p. 125. <https://doi.org/10.1126/science.ns-3.52.122>.

![An Orthoceratite (top left), an ammonite (_Asteroceras obtusum,_ bottom left), and a nautilus (_Nautilus pompilius,_ right; left two images CC-BY; by Nobu Tamura; right image public domain; all Wikimedia Commons)](cephalopods.jpg){.lgimg width=100%}

The first example that he offers us here, and the one that we will unpack, concerns the following evolutionary story. Some of the most commonly found fossils in the fossil record are a sort of spiral-shaped cephalopod called the ammonites, which lived for hundreds of millions of years before going extinct at the same time as the dinosaurs. They are such commonly found fossils that they were described by the ancient Roman author Pliny, and finding an ammonite can be a way for paleontologists to tell just how old a layer of rock actually is. With their closely related group the nautiluses (a few of which are still around today), they would have been a common sight in the ocean for a vast amount of the earth's history.

:::: think
**THINK[5]:** The fossil record is an important source of data for our understanding of evolution, but it is also quite strange. What do you think might be some advantages and disadvantages of using fossil data to support a conjecture in evolution? How might we correct some of those disadvantages with other kinds of experiments to produce a more robust hypothesis?
::::

![Alpheus Hyatt, in an engraving from around 1885 in _Popular Science Monthly_ (public domain; Wikimedia Commons)](hyatt.jpg){.smallimg .left width=33%}

How did they evolve? The ancestor to all of the ammonites and nautiluses was a group of straight-shelled organisms, called the Orthoceratites. It is this transition, from the straight-shelled Orthoceratites which lived in open water, to the much more diverse ammonites and nautiluses, that interests Hyatt. He argues that the evolution of these two groups could not have been caused by natural selection. A better explanation, he argues, is the direct interaction between organisms and their environment. As the environment changed around them, or the ancestor organisms had reasons to move to different environments, they began to struggle to survive in their new homes. The fact that the former, straight-shelled form, which had been very efficient in open water, *needed* (in one case) to be able to live in a more diverse environment (sometimes swimming, sometimes at the bottom of the ocean, sometimes in shallower water) led it to evolve into the nautilus, and the fact that it *needed* (in another case) to live in shallow, coastal areas led it to evolve into the ammonites.

Of course, Hyatt doesn't think that a marine invertebrate is actually thinking about *what it wants to do* -- it's not sitting in the deep water thinking "I really wish I could live near the shore." As he puts it:

:::: {.quote title="Hyatt, from the journal \emph{Science} (1884)"}
We cannot seriously imagine these changes to have resulted from intelligent effort; but we can fully join Lamarck, Cope, and Ryder, in imagining them as due to efforts induced by the physical requirements of the habitat, and think this position to be better supported by facts than any other hypothesis. (Footnote: A noted French writer well known to embryologists, Lacaze-Duthier, has lately asked, "Who, at the present time, supports Lamarck?" The author can answer, that some of our leading scientific men consider Lamarck's hypothesis to contain more fundamental truths than Darwin's or any other.)[^hyatt2]
::::

[^hyatt2]: Hyatt, p. 125.

:::: think
**THINK[6]:** Hyatt is openly making an appeal to persuasion here. In placing himself in opposition to Darwin, he explicitly lists the names of authorities -- Lamarck himself, as well as Cope and Ryder -- who agree with him, calling them "some of our leading scientific men."

What do you think this says about Hyatt's theory and his position within the scientific establishment? How should we understand these arguments that appeal directly to the authority of other scientists? When do you think that they might be valid?
::::

What the Orthoceratites are doing, though, is struggling against their habitat. Some of them found themselves in shallow water, and thus began causing changes in themselves that, when passed on to offspring for many generations, created the ammonites. Hyatt is very clear, however, about what this means: Lamarck, he argues, has described "more fundamental truths" than Darwin has. The force of the organism's struggle against its environment is much more powerful, Hyatt thinks, than natural selection.

![Edward Drinker Cope, in an engraving from 1881 in _Popular Science Monthly_ (public domain; Wikimedia Commons)](cope.jpg){.smallimg width=33%}

In addition to Lamarck, Hyatt mentions two other figures as proponents of neo-Lamarckism, one of whom was [Edward Drinker Cope](https://en.wikipedia.org/wiki/Edward_Drinker_Cope) (1840--1897). Cope is an amazing figure, worth studying for lots of reasons. Among other things, he was one of the first major American paleontologists, and a dispute between him and fellow fossil-hunter [Othniel Charles Marsh](https://en.wikipedia.org/wiki/Othniel_Charles_Marsh) (1831--1899) kicked off a competition to find dinosaur fossils in the American West that became known as the [Bone Wars.](https://en.wikipedia.org/wiki/Bone_Wars) This radically changed our understanding of the nature and evolution of dinosaurs, and helped to create the contemporary discipline of paleontology.

But in addition to that, Cope was another prominent supporter of neo-Lamarckism. For him, neo-Lamarckism was a way to understand a different feature of the fossil record than the one that was important for Hyatt. In his words:

:::: {.quote title="Cope, \emph{Origin of the Fittest} (1887)"}
It is sufficiently well known that the essential features of a majority of genera are not adaptive in their natures, and that those of many others are so slightly so, as to offer little ground for the supposition that this necessity has preserved them.

Both laws [that is, natural selection, and another evolutionary force proposed by Cope] must be subordinate to that unknown force which determines the direction of the great series. If a series of suppressions of the nervous and circulatory systems of beings of common birth produced the "synthetic" predecessors of the classes of Vertebrata, the direction toward which the highest advanced, or its ultimate type, can be only ascribed as yet to the divine fiat. So far as we can see, there is no reason or law to produce a preference for this direction above any other direction.[^cope]
::::

[^cope]: Cope, Edward Drinker. 1887. [_The Origin of the Fittest._](https://doi.org/10.5962/bhl.title.24160) New York: D. Appleton and Company, pp. 106--7.

For Cope, then, the important thing that neo-Lamarckism lets us understand is the fact that many of the "essential features" of various groups of organisms seem not to be adaptive, and therefore not to have been created by natural selection. Despite this, however, they have been preserved, sometimes for a very long time, in the history of life. (We will return to other explanations for non-adaptive features in the next reading, on the related concept of *orthogenesis.*) For Cope, then, the best way to explain this, in the same manner that Lamarck had done, is to interpret the series of organisms across evolutionary history as having a sort of inherent "direction." Unlike Lamarck, Cope does not want to say that this direction points toward "progress" or "complexity" or "higher" organisms. It is often, as he says here, hard for us to even clearly understand how that direction should be explained, unless by "divine fiat." What we need to do, he argues, is attempt to understand the "unknown force" that drives the Lamarckian, non-adaptive change in different groups of organisms. That, rather than natural selection, is the most important process in evolutionary theory.

:::: think
**THINK[7]:** This leaves us three alternatives in play for the explanation of these non-adaptive characters. One is a classic, Lamarckian force of upward progress caused by struggle with the environment (as supported also by Hyatt). One is an unknown force driving organisms in particular directions, but without any invocation of adaptation (as supported by Cope). Lastly we have Darwin, who would presumably have appealed to natural selection to explain these changes.

How should we compare these possibilities? What kinds of data does each explanation have to support it? What features of the natural world does each leave unexplained or unconsidered? Which do you think would have made the most sense had you been asked to choose in the late-19th century?
::::

\enlargethispage{\baselineskip}

Let's think first a bit about the positive view of the neo-Lamarckians, before we turn to what their perspective might have to tell us about our view of evolution today. It is no accident that many of the most prominent neo-Lamarckians were paleontologists, looking at long-term changes in groups in the fossil record. When we look at series of fossils as their characteristics change over time, we often are struck that there seems to be linear progression in certain features, in a way that doesn't seem necessarily to be connected with any selective pressure. Whatever the force would be that controlled the appearance of progression like this -- Hyatt, with Lamarck, though that it was the active efforts of organisms to respond to the needs they had within their environments, while Cope didn't know what it was, but hoped we would one day find out -- it is the main driver of change in species, especially over geologic time.

![Cope's sketch of four dinosaurs. Problems abound, including, for the dinosaur on the front in the water, the head's being placed on the end of its tail! From an article in the journal _American Naturalist_ (public domain; Wikimedia Commons)](cope_sketch.jpg){.medimg .left width=66%}

Darwin, for his part, knew about cases like this. He had argued that they should be understood in a very different way. Certain parts, he wrote in the _Origin,_ are correlated with one another in the way that they grow and develop. So natural selection, working on one part that was important for an organism's survival, might "accidentally" (so to speak) cause changes in a different part, if the two parts' growth were linked. That could mean that natural selection might appear to be working on characters that weren't actually relevant for an organism's success in its current environment, which might produce the appearance of non-adaptive evolution. (Much more on this in the next reading.)

But if the neo-Lamarckian explanation was valid, why are we reading about their theories as a misunderstanding of evolution? What happened to neo-Lamarckism? In fact, the real reason that it declined in importance was a persistent failure to replicate its results experimentally. If organisms are really responding to the needs of their environments in a way that is inherited by their offspring, then placing a population of organisms in a new environment, over a long period of time, should cause the descendants to eventually be born with modified features, more suitable to the new environment in which they were placed, variations that weren't present in the population before, and only exist now because of the organisms' having struggled in their new environment. This kind of experimental evidence, however, simply never materialized, though the experiments were tried across a wide variety of different species.

:::: think
**THINK[8]:** Imagine that you were a neo-Lamarckian, and you were presented with the following experimental result. Rats have been trained to solve a maze for multiple generations in the lab, and in spite of this, their descendants don't wind up solving the maze any faster than their ancestors. What kinds of responses might you have? How could you still defend your theory? More broadly, when do you think a scientist would *have* to admit defeat, to concede that their theory had failed? How much evidence, and evidence of what kinds, would be required?
::::

What can we learn from the episode of neo-Lamarckism that could be helpful for understanding evolutionary theory today? First, it cautions us that it can be extremely difficult to interpret the patterns that we see in the fossil record. There are many different ways in which such a pattern might have been generated, different targets of natural selection, and different modes of interaction between organisms and their environments. Sometimes, something that looks like a pattern to us might not even have been evolutionarily generated at all: it might, for example, just be the result of [the forces of physics to which an organism is subject while it is growing.](https://phys.org/news/2019-12-math-mechanics-bivalve-shells.html)

One way of understanding the critiques of the neo-Lamarckians demonstrates a point about evolution that has become more and more important in recent years. Something that neo-Lamarckian evolution was supposed to give us was the ability for organisms to respond quickly when changes in their environments made their previous form of life difficult or impossible. The Orthoceratites, that is, saved themselves from extinction by successfully struggling with their environment to change their features, turning into ammonites or nautiluses. Some recent work in evolution has returned to this insight, focusing on the ways in which a concept now known as [*phenotypic plasticity*](https://www.sciencedirect.com/topics/agricultural-and-biological-sciences/phenotypic-plasticity) might play a similar role. The idea here is that an organism might not be selected for a particular, permanent solution to a problem -- rather, in some circumstances, it might be better to simply remain flexible, particularly if the environment is highly variable. Only later, when the changing environment "settles down," would natural selection arrive to make some of these flexible outcomes more permanent. By taking this worry more seriously, we were attuned to a variety of really interesting phenomena connecting evolution, molecular biology, and organismic development. In that sense, the neo-Lamarckians were right -- environmental change is a real and important problem that organisms have to be able to solve, and there are interesting theoretical questions at work about how natural selection has managed to solve it.

:::: think
**THINK[9]:** We not infrequently see examples like this, where theories that were thought to be long since disproven gain a new life as they are reinterpreted in light of fresh data. What do you think this might tell us about the relationship between the history of science and the practice of science today? Should we encourage scientists to learn more about history, or would that be a waste of time?
::::

Finally, it's worth noting that contemporary scientific research has offered us some insights into very particular ways that, in fact, acquired characters might actually be passed on to offspring. To take just one example, some behaviors in life might cause certain kinds of changes in DNA, *other* than mutations in the actual genetic sequence -- such as changes in [methylation patterns](https://en.wikipedia.org/wiki/DNA_methylation) -- that could still be passed on and govern certain future outcomes in the lives of offspring. While nothing like Lamarck's complete theory can be vindicated, perhaps the examples here will need to be updated soon!

:::: think
**THINK[10]:** Do you think it is helpful to refer to contemporary results in molecular biology in terms of old theoretical names like "neo-Lamarckism?" Give some reasons that this identification might be both confusing and helpful.
::::


\enlargethispage{\baselineskip}

## THINK: NOS Reflection Questions:

What does the story of neo-Lamarckism tell us about the following features of the nature of science?

* evidential relevance (empiricism)
* role of cultural beliefs (national identity)
* interdisciplinary thinking
* role of probability in inference
* role of imagination and creative synthesis
* resolving disagreement


## Further Reading

* Bowler, Peter J. 1992. [_The Eclipse of Darwinism: Anti-Darwinian Evolution Theories in the Decades around 1900._](https://www.worldcat.org/title/eclipse-of-darwinism-anti-darwinian-evolution-theories-in-the-decades-around-1900/oclc/26547189) Baltimore, MD: Johns Hopkins University Press.
* Bowler, Peter J. 2017. "American Palaeontology and the Reception of Darwinism." _Studies in History and Philosophy of Biological and Biomedical Sciences_ 66: 3--7. <https://doi.org/10.1016/j.shpsc.2017.06.002>.
* Jaffe, Mark. 2000. [_The Gilded Dinosaur: The Fossil War between E. D. Cope and O. C. Marsh and the Rise of American Science._](https://www.worldcat.org/title/gilded-dinosaur-the-fossil-war-between-ed-cope-and-oc-marsh-and-the-rise-of-american-science/oclc/890171516) New York: Crown.
* Haig, David. 2007. "Weismann Rules! OK? Epigenetics and the Lamarckian Temptation." _Biology & Philosophy_ 22 (3): 415--28. <https://doi.org/10.1007/s10539-006-9033-y>.

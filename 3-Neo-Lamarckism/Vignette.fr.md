---
lang: fr
---

# 3. Lamarck et la transmission des caractères acquis

Quand on se penche sur le phénomène du changement dans le monde naturel, il est difficile de ne pas relever certaines adaptations surprenantes. Prenons l'exemple sans doute le plus courant, celui du cou de la girafe. Évidemment, il est très avantageux pour une girafe d'être capable de manger les feuilles qui se trouvent en haut des arbres. Tout aussi évidemment, la girafe **aimerait** pouvoir accéder à plus de nourriture. Il est donc parfaitement naturel de décrire ce qui aurait pu se passer chez les girafes de la façon suivante : les girafes tendaient le cou de manière répétée en essayant désespérément de manger les feuilles les plus hautes et en retour, leur cou a grandi. On voit même des cas semblables dans la vie quotidienne : si vous soulevez beaucoup de poids de musculation, la réaction de vos muscles est de grossir. Pourquoi l'évolution ne fonctionnerait-elle pas comme ça ?

:::: think
**RÉFLEXION 1 :** Pour beaucoup de scientifiques à travers les siècles, cette conclusion paraissait évidente. Pourtant, elle peut nous sembler étrange aujourd'hui. Que pensez-vous de cette vision des choses ? Plus généralement, dans quels cas les informations tirées de notre vie quotidienne sont-elles utiles comme source de savoir scientifique et dans quels cas sont-elles trompeuses ? Comment faire la différence ?
::::

![Une girafe déguste des feuilles très haut placées, Parc national Kruger, Afrique du Sud (image : CC-BY-SA, Esculapio, Wikimedia Commons)](giraffe.jpg){.smallimg width=33%}

Pourtant, il se trouve que l'évolution ne fonctionne **pas** comme ça (ou du moins presque jamais comme ça, nous y reviendrons) mais ce type de malentendu sur le processus évolutif est un des plus anciens et des plus répandus. Pendant plus d'un siècle, beaucoup de scientifiques de premier plan avaient la conviction que, pour décrire le problème à leur manière, **les caractères acquis sont hérités**, c'est-à-dire que les caractères développés par les organismes au cours de leur vie (comme un cou allongé ou de gros biceps) seront transmis à leur descendance. Darwin disait même que ce qu'il appelait « les effets de l'usage et du non-usage », c'est-à-dire la manière dont l'utilisation d'un organe ou le non-recours à un autre organe pouvaient affecter les modalités de transmission des caractères des parents à leurs descendants, étaient un facteur important dans les cas comme la disparition des yeux chez les espèces vivant dans des grottes.

Le zoologue français [Jean-Baptiste de Lamarck](https://fr.wikipedia.org/wiki/Jean-Baptiste_de_Lamarck) (1744-1829) fut un des premiers auteurs à élaborer une théorie complète du monde organique allant dans ce sens ; son nom y est depuis resté attaché. On appelle parfois sa vision du développement de la vie le « [transformisme](https://fr.wikipedia.org/wiki/Transformisme_(biologie)) » pour indiquer que bien qu'il ait considéré que les espèces changeaient, il n'avait pas de théorie de « l'évolution » au sens moderne du terme. Au contraire, à son idée, la vie était constamment en pleine création tout autour de nous, débutant par des micro-organismes très simples qui se transformaient à un rythme régulier en êtres plus avancés et plus complexes (rappelez-vous notre premier cours sur Darwin et les concepts « d'inférieur et supérieur »). Donc les micro-organismes, en partie parce qu'ils s'efforçaient d'accéder à une vie meilleure, se transformaient en méduses, qui devenaient des vers puis, selon un progrès constant, des insectes, des mollusques et finalement des vertébrés comme nous.

:::: think
**RÉFLEXION 2 :** Lamarck est intéressant aussi pour le lien entre fierté scientifique et fierté nationale. Pendant des années en France, on a considéré que Lamarck était en grande partie le créateur de la théorie de l'évolution et que Darwin s'en était attribué illégitimement la reconnaissance. Qu'est-ce qui est différent entre l'opinion de Darwin et l'image de la théorie de Lamarck illustrée ici ? Comment l'invention de théories scientifiques devient-elle une question d'orgueil national ou un contentieux entre pays ?
::::

Dans son ouvrage principal sur la biologie, la *Philosophie zoologique* publiée en 1809, Lamarck expose la transmission héréditaire des caractères acquis selon les termes de ce qu'il nomme deux lois de la biologie :

:::: {.quote title="Lamarck, \emph{Philosophie zoologique} (1809)"}
*Première loi*

Dans tout animal qui n'a point dépassé le terme de ses développements, l'emploi plus fréquent et soutenu d'un organe quelconque fortifie peu à peu cet organe, le développe, l'agrandit et lui donne une puissance proportionnée à la durée de cet emploi, tandis que le défaut constant d'usage de tel organe l'affaiblit insensiblement, le détériore, diminue progressivement ses facultés et finit par le faire disparaître.

*Deuxième loi*

Tout ce que la nature a fait acquérir ou perdre aux individus par l'influence des circonstances où leur race se trouve depuis longtemps exposée et, par conséquent, par l'influence de l'emploi prédominant de tel organe ou par celle d'un défaut constant d'usage de telle partie, elle le conserve par la génération aux nouveaux individus qui en proviennent, pourvu que les changements acquis soient communs aux deux sexes ou à ceux qui ont produit ces nouveaux individus.

Ce sont là deux vérités constantes qui ne peuvent être méconnues que de ceux qui n'ont jamais observé ni suivi la nature dans ses opérations \[\...\].[^lamarck]
::::

[^lamarck]: Jean-Baptiste Lamarck, [*Philosophie zoologique*](https://doi.org/10.5962/bhl.title.101794) (1809), 1^e^ tome, 2^e^ édition, Paris, 1830, éd. Germer Baillère, p. 235. La ponctuation a été modernisée dans l'extrait présenté ici.

La première loi, donc, affirme que toute partie d'un organisme utilisée fréquemment deviendra plus grande et plus forte à cause de la répétition à long terme des mêmes actions. Le non-usage d'autres parties les fera rapetisser voire même disparaître. La deuxième loi dit que ces changements seront hérités d'une manière telle qu'après un temps suffisant, ils feront définitivement partie de l'espèce : le cou étiré de la girafe deviendrait donc, avec assez de temps, une caractéristique permanente de toutes les girafes.

Bien que la transmission des caractères acquis n'ait pas été pour Lamarck le seul mécanisme de changement des espèces (comme Darwin, il croyait également que des changements dans l'environnement d'un organisme auraient une grande importance), elle a été associée si étroitement à la théorie de Lamarck qu'on en est venu, même de nos jours, à appeler [lamarckisme](https://www.sciencedirect.com/topics/agricultural-and-biological-sciences/lamarckism) l'hérédité des caractères acquis.

Même si, nous l'avons déjà dit, Darwin lui-même pensait que les caractères acquis étaient hérités, l'idée a été de plus en plus contestée dans les années qui ont suivi la publication de *L'origine des espèces*. Le biologiste allemand [August Weismann](https://fr.wikipedia.org/wiki/August_Weismann) (1834-1914) fut le premier à suggérer que les cellules sexuelles comme les ovules ou les spermatozoïdes, issues de la lignée germinale, étaient [séparées du reste des cellules de l'organisme](http://embryo.asu.edu/handle/10776/8284), ou cellules somatiques, et qu'aucun changement présenté par les cellules somatiques ne pouvait être hérité car seules les cellules germinales étaient responsables de la production de la génération suivante d'organismes. Bien que cette théorie ait été vivement débattue à l'époque (jusqu'à ce que des avancées ultérieures en biologie cellulaire permettent de comprendre comment les embryons se développent à partir des gamètes), elle entama sérieusement la crédibilité chez les biologistes (ou du moins une partie des biologistes) de la théorie de la transmission des caractères acquis.

Mais tous ne furent pas dissuadés (nous en étudierons certains dans ce cours et d'autres dans le cours suivant). Un groupe, majoritairement américain, défendait le lamarckisme pour répondre à ce qu'il percevait comme deux défauts majeurs dans le système de Darwin : le caractère aléatoire de la variation et le temps, long, qu'il fallait à ces variations pour s'accumuler.

Examinons les deux problèmes qui les font réagir. Tout d'abord, ces biologistes objectent qu'il existe des preuves que la variation n'est pas aléatoire et que les organismes, comme le disait Lamarck, **réagissent** à leur environnement et, de là, varient précisément dans la direction nécessaire pour régler les difficultés environnementales qu'ils ont rencontrées. Pour Darwin, ce type de variation était impossible. La sélection naturelle était pour lui le seul élément de l'évolution orienté dans le sens d'une plus grande adaptation. La deuxième objection, sur la vitesse d'évolution, était répandue juste après la sortie de *L'origine*. On pensait à l'époque que la Terre était **beaucoup** plus jeune que nous ne le pensons aujourd'hui : comme on ne comprenait pas encore la radioactivité, on pensait que [la Terre avait refroidi bien plus vite après son stade initial de matière en fusion](https://www.worldcat.org/title/lord-kelvin-and-the-age-of-the-earth/oclc/892196357). Les biologistes s'inquiétaient donc qu'il n'y ait pas eu assez de temps pour que la sélection naturelle agisse et produise le type d'organismes présents à notre époque.

:::: think
**RÉFLEXION 3 :** Nous avons vu qu'un aspect important de la validation de la théorie de l'évolution était le calcul de l'âge de la Terre, dérivé de la physique (et plus tard de la découverte de la radioactivité). Comment pensez-vous que des connaissances élaborées en physique peuvent se raccorder à des thèses biologiques ? Dans quelles situations seraient-elles utiles ? Voyez-vous des circonstances où faire appel à d'autres sciences n'apporte rien ou pose problème ?

**RÉFLEXION 4 :** L'idée de « variation aléatoire » a toujours été difficile à interpréter dans la théorie de l'évolution. Darwin entend par là que les variations ne sont pas influencées dans la direction de ce que « veut » l'organisme mais plutôt qu'elles se produisent sans considération de leurs avantages ou leurs inconvénients. La sélection naturelle, au contraire, est tout sauf aléatoire, avec sa capacité à pousser les populations dans le sens d'une plus grande adaptation.

Quels malentendus ou erreurs d'interprétation peuvent découler de la description de la variation comme « aléatoire » ? Quelle différence la présence de la sélection naturelle au sein du processus évolutif fait-elle ? D'une manière générale, quelles peuvent être les différences entre les théories scientifiques qui décrivent leurs résultats en termes de probabilités et celles, comme la physique newtonienne, qui décrivent précisément ce qui va se produire ?
::::

Pour ces scientifiques, qu'on appellerait bientôt les néo-lamarckiens, les deux lois de Lamarck, ou quelque chose d'approchant, résoudraient ces deux problèmes. Parmi ces scientifiques, [Alpheus Hyatt](https://fr.wikipedia.org/wiki/Alpheus_Hyatt) (1838-1902) tirait la plupart de ses connaissances empiriques des invertébrés marins. Il disait que l'évolution des céphalopodes (tels que les calmars, pieuvres et seiches d'aujourd'hui) apportait la meilleure preuve de la véracité des principes de Lamarck. Regardons d'abord ce qu'il a écrit dans un article de revue scientifique en 1884 :

:::: {.quote title="Hyatt, de la revue \emph{Science} (1884)"}
Les *efforts* de l'orthocère pour s'adapter complètement aux exigences d'un habitat mixte ont donné au monde les nautiloïdes : les *efforts* du même type pour se transformer totalement en un rampant littoral ont abouti aux ammonites. Les formes successives des bélemnites sont apparues de la même manière mais là, l'habitat proche du fond et la complète adaptation à la nage dans cet habitat étaient l'objectif, tandis que les sépiides représentent les buts les plus élevés ainsi que les plus hauts accomplissements des orthocères dans leur formes rapaces nageant en surface.[^hyatt1]
::::

[^hyatt1]: Alpheus Hyatt, « The Evolution of the Cephalopoda.---I », *Science*, 1884, vol. 3, n^o^ 52, p. 122-127, <https://doi.org/10.1126/science.ns-3.52.122>, p. 125. Traduction de l'anglais : Sandra Mouton.

![En haut à gauche : un orthocère, en bas à gauche : une ammonite *Asteroceras obtusum* (images : CC-BY, Nobu Tamura, Wikimedia Commons), à droite : un nautile *Nautilus pompilius* (image : domaine public, Wikimedia Commons)](cephalopods.jpg){.lgimg width=100%}

Le premier exemple qui nous est proposé ici par Hyatt, et celui sur lequel nous nous concentrerons, concerne l'anecdote suivante sur l'évolution. Parmi les fossiles les plus souvent découverts se trouve un type de céphalopode en forme de spirale appelé ammonite. Ces organismes ont vécu des centaines de millions d'années avant de s'éteindre en même temps que les dinosaures. Ces fossiles sont si fréquents qu'ils ont même été décrits par l'auteur romain antique Pline et trouver une ammonite peut être un moyen pour les paléontologues de dater une strate rocheuse. Avec leurs proches parents du groupe des nautiles (dont certains existent encore), elles étaient abondantes dans les océans pendant une longue période de l'histoire de la Terre.

:::: think
**RÉFLEXION 5 :** Les fossiles sont une source de données importante pour notre compréhension de l'évolution mais assez étrange. Quels peuvent être les avantages et les inconvénients de l'utilisation de données fossiles pour étayer une conjecture en théorie de l'évolution ? Comment remédier à certains de ces inconvénients grâce à d'autres types d'expériences pour produire une hypothèse plus solide ?
::::

![Alpheus Hyatt, gravure, autour de 1885, publiée dans *Popular Science Monthly* (image : domaine public, Wikimedia Commons)](hyatt.jpg){.smallimg .left width=33%}

Quelle a été l'évolution des ammonites ? L'ancêtre de toutes les ammonites et tous les nautiles était un groupe d'organismes à coquille droite, les orthocères ou *Orthoceras*. C'est cette transition, des orthocères à coquille droite, qui vivaient en eau libre, aux ammonites et nautiles, beaucoup plus diversifiés, qui intéresse Hyatt. Il avance que l'évolution de ces deux groupes ne peut pas avoir été causée par la sélection naturelle. D'après lui, une meilleure explication est l'interaction directe entre les organismes et leur environnement.

À mesure que l'environnement se modifiait autour d'eux ou que les organismes ancêtres avaient des raisons de se déplacer vers de nouveaux environnements, ils devaient lutter pour survivre dans leur nouvel habitat. Le fait que la forme ancienne, à coquille droite, qui avait été très efficace en eau libre, **avait besoin** (dans un cas) de pouvoir vivre dans un environnement plus diversifié (nageant par moments, au fond de l'océan à d'autres, parfois dans des eaux moins profondes) l'a conduite à évoluer en nautile et le fait que (dans un autre cas) elle **avait besoin** de vivre dans les eaux côtières peu profondes l'a conduite à évoluer en ammonite.

Bien sûr, Hyatt ne pense pas qu'un invertébré marin réfléchit effectivement à **ce qu'il veut faire** : il n'est pas assis au fond de l'eau à se dire « ce serait vraiment bien de pouvoir vivre près de la côte ». Comme Hyatt le dit :

:::: {.quote title="Hyatt, de la revue \emph{Science} (1884)"}
Nous ne pouvons sérieusement imaginer que ces changements soient le résultat d'un effort intelligent mais nous pouvons sans réserve rejoindre Lamarck, Cope et Ryder en imaginant qu'ils sont dus aux efforts occasionnés par les contraintes physiques de l'habitat et considérer que cette position est mieux supportée par les faits que toute autre hypothèse. (*Note de bas de page* : un auteur français notable bien connu des embryologues, Lacaze-Duthier, a demandé récemment : « Qui, à l'heure actuelle, soutient Lamarck ? ». L'auteur peut répondre que certains de nos plus grands hommes de science considèrent que l'hypothèse de Lamarck contient plus de vérités fondamentales que celle de Darwin ou que toute autre.)[^hyatt2]
::::

[^hyatt2]: Hyatt, « The Evolution of the Cephalopoda.---I », p. 125.

:::: think
**RÉFLEXION 6 :** Hyatt fait ouvertement appel à des techniques de persuasion ici. Dans sa déclaration d'opposition à Darwin, il liste explicitement le nom des autorités reconnues, Lamarck lui-même, Cope et Ryder, qui sont d'accord avec lui et les appelle « certains de nos plus grands hommes de science ».

D'après vous, qu'est-ce que cela nous dit sur la théorie de Hyatt et sur la position de celui-ci par rapport aux scientifiques de renom ? Comment devrions-nous interpréter ces arguments qui invoquent directement l'autorité d'autres scientifiques ? Dans quels cas sont-ils valides ?
::::

Quant aux orthocères, ils luttent pour s'adapter à leur habitat. Certains se retrouvent dans des eaux peu profondes et commencent donc à provoquer en eux-mêmes des changements qui, transmis à la descendance sur de nombreuses générations, ont donné naissance aux ammonites. Hyatt expose très clairement ce que cela implique : pour lui, Lamarck a décrit « plus de vérités fondamentales » que Darwin. Dans l'esprit de Hyatt, l'impulsion donnée par la lutte de l'organisme face à son environnement est bien plus puissante que la sélection naturelle.

![Edward Drinker Cope, gravure de 1881, publiée dans *Popular Science Monthly* (image : domaine public, Wikimedia Commons)](cope.jpg){.smallimg width=33%}

En plus de Lamarck, Hyatt présente deux personnages en partisans du néo-lamarckisme, notamment [Edward Drinker Cope](https://fr.wikipedia.org/wiki/Edward_Drinker_Cope) (1840-1897). Cope est un personnage extraordinaire, qui vaut qu'on s'y intéresse pour de multiples raisons. Entre autres choses, il fut un des premiers grands paléontologues américains et un désaccord entre lui et son collègue chasseur de fossiles [Othniel Charles Marsh](https://fr.wikipedia.org/wiki/Othniel_Charles_Marsh) (1831-1899) donna lieu à une course à la découverte de fossiles de dinosaures dans l'Ouest américain, connue sous le nom de [guerre des os](https://fr.wikipedia.org/wiki/Guerre_des_os). Notre vision de la nature et de l'évolution des dinosaures en a été révolutionnée et l'épisode a contribué à la création de la discipline contemporaine de paléontologie.

En plus de tout cela, Cope était un éminent défenseur du néo-lamarckisme. Pour lui, cette approche permettait de comprendre une autre caractéristique des fossiles que celle qui préoccupait Hyatt. Selon ses propres termes :

:::: {.quote title="Cope, \emph{L'origine des plus aptes} (1887)"}
Il est suffisamment connu que les caractéristiques essentielles d'une majorité de genres ne sont pas de nature adaptative et que celles de beaucoup d'autres \[genres\] le sont si légèrement qu'on ne peut y appuyer la supposition que c'est cette raison nécessaire qui les a préservées.

Les deux lois \[à savoir la sélection naturelle et une autre force évolutive proposée par Cope\] doivent être subordonnées à cette force inconnue qui détermine l'orientation de la grande série. Si une succession d'éliminations des systèmes nerveux et circulatoire d'êtres aux origines communes a produit les prédécesseurs « combinés » des classes de vertébrés, la direction prise par l'organisme le plus avancé, ou son type ultime, ne peut à ce jour être attribuée qu'au décret divin. Pour autant que nous pouvons le voir, il n'existe pas de raison ni de loi qui cause une préférence pour cette direction par-dessus toutes les autres.[^cope]
::::

[^cope]: Edward Drinker Cope, [*The Origin of the Fittest*](https://doi.org/10.5962/bhl.title.24160), New York, 1887, éd. D. Appleton and Company, p. 106-107. Traduction de l'anglais : Sandra Mouton.

De l'avis de Cope, le néo-lamarckisme nous permet d'appréhender le fait important que beaucoup des « caractéristiques essentielles » de divers groupes d'organismes ne semblent pas être adaptatives et donc ne semblent pas avoir été créées par sélection naturelle. Malgré cela, elles ont été préservées, parfois sur de très longues durées, dans l'histoire du vivant. (Nous reviendrons aux autres explications des caractéristiques non adaptatives au cours suivant sur le concept, lié, d'orthogénèse.) Pour Cope, la meilleure explication, à la manière dont l'a fait Lamarck, est d'interpréter la série des organismes à travers l'histoire de l'évolution comme étant dotée d'une sorte de « direction » intrinsèque.

Mais contrairement à Lamarck, Cope veut éviter de dire que cette direction va dans le sens du « progrès » ou de la « complexité » ou d'organismes « supérieurs ». Comme il le mentionne, il est souvent difficile de voir la cause de cette direction, sauf à l'expliquer par un « décret divin ». D'après Cope, il nous faut tenter de comprendre la « force inconnue » qui alimente les changements lamarckiens non adaptatifs dans différents groupes d'organismes : cette force-là, plutôt que la sélection naturelle, est le processus le plus important pour la théorie de l'évolution.

:::: think
**RÉFLEXION 7 :** Ceci nous laisse trois possibilités différentes pour expliquer ces caractères non adaptatifs. L'une est une force lamarckienne classique d'avancée vers le progrès causée par la lutte face à l'environnement (comme le pensait aussi Hyatt). La deuxième est une force inconnue poussant les organismes dans des directions spécifiques mais sans recours à l'adaptation (c'est l'opinion de Cope). Enfin, nous avons Darwin, dont on peut imaginer qu'il aurait invoqué la sélection naturelle pour expliquer ces changements.

Comment devrions-nous comparer ces possibilités ? Quels types de données viennent appuyer chaque explication ? Quels éléments du monde naturel sont inexpliqués ou laissés de côté par chaque théorie ? Laquelle aurait été la plus logique si on vous avait demandé de choisir à la fin du XIX^e^ siècle ?
::::

Réfléchissons tout d'abord à la position avancée par les néo-lamarckiens avant de nous pencher sur ce que leur point de vue peut nous apporter sur notre vision actuelle de l'évolution. Ce n'est pas un hasard si beaucoup de néo-lamarckiens les plus célèbres étaient paléontologues, intéressés par les changements à long terme dans les groupes représentés parmi les fossiles excavés. Quand on examine une série de fossiles et les modifications de leurs caractéristiques au cours du temps, on est souvent frappé par une apparente progression linéaire de certaines caractéristiques, sans qu'il y ait nécessairement de lien évident avec des pressions sélectives. Quelle que soit la force contrôlant une apparente progression comme celle-là (Hyatt et Lamarck pensaient que c'étaient les efforts actifs des organismes pour répondre aux besoins qu'ils ressentaient dans leur environnement ; Cope, lui, ne savait pas ce que c'était mais espérait qu'on le découvrirait un jour), elle est la cause principale de changement des espèces, en particulier en temps géologique.

![Dessin de quatre dinosaures, par Cope. Les problèmes sont nombreux, notamment chez le dinosaure dans l'eau au premier plan, dont la tête est au bout de la queue ! Extrait d'un article paru dans la revue *American Naturalist* (image : domaine public, Wikimedia Commons)](cope_sketch.jpg){.medimg .left width=66%}

Darwin, lui, connaissait des cas de ce type. Il avait avancé qu'ils devaient être compris complètement différemment. Il avait écrit dans *L'origine* que certaines parties du corps sont liées entre elles dans leur mode de croissance et de développement. La sélection naturelle, agissant sur une partie importante pour la survie d'un organisme, pouvait donc causer « accidentellement », en quelque sorte, des changements dans une autre partie, si le développement des deux parties était lié. La sélection naturelle pouvait donc avoir l'air d'agir sur des caractères sans importance pour la réussite d'un organisme dans son environnement d'alors, ce qui donnait éventuellement l'apparence d'une évolution non adaptative. (Ceci sera développé dans le cours suivant.)

Mais pourquoi, si l'explication néo-lamarckienne était valide, entendons-nous parler de ces théories comme des erreurs d'interprétation de l'évolution ? Qu'est-il arrivé au néo-lamarckisme ? À vrai dire, la raison de son déclin est son échec durable à reproduire expérimentalement ses résultats. Si les organismes réagissent effectivement aux exigences de leur environnement d'une façon qui est transmise à leur descendance, alors placer une population dans un nouvel environnement, sur une longue période, devrait conduire au bout d'un moment à la naissance de descendants porteurs de caractéristiques modifiées, mieux adaptées au nouvel environnement où ils se trouvent, de variations qui n'étaient pas présentes avant dans cette population et n'existent désormais que parce que les organismes ont dû s'adapter à leur nouvel environnement. Ce type de preuve expérimentale ne s'est toutefois jamais matérialisé, bien que les expériences aient été effectuées sur un large éventail d'espèces différentes.

:::: think
**RÉFLEXION 8 :** Imaginez-vous dans la peau d'un néo-lamarckien confronté aux résultats expérimentaux suivants : des rats ont été entraînés en laboratoire sur de multiples générations à sortir d'un labyrinthe et malgré ça, leurs descendants ne finissent pas par sortir plus vite que leurs ancêtres. Quelles réponses pourriez-vous avoir ? Comment pourriez-vous continuer à défendre votre théorie ? Plus généralement, à quel moment d'après vous **faut-il** que les scientifiques admettent leur défaite et l'échec de leur théorie ? Quelle quantité de preuves faudrait-il et de quels types ?
::::

Ensuite, qu'est-ce que l'épisode du néo-lamarckisme a d'utile à nous apprendre, pour notre compréhension de la théorie de l'évolution aujourd'hui ? Tout d'abord, il nous avertit qu'il peut être très difficile d'interpréter les régularités que nous voyons dans les fossiles. Nombreuses et variées sont les voies pouvant avoir généré une telle configuration, diverses les cibles de sélection naturelle et divers les modes d'interaction entre les organismes et leur environnement. Parfois, ce qui à nos yeux ressemble à un schéma régulier n'est même pas le fruit de l'évolution : il peut s'agir, par exemple, du résultat des [forces physiques auxquelles l'organisme a été soumis pendant sa croissance.](https://phys.org/news/2019-12-math-mechanics-bivalve-shells.html)

Les critiques des néo-lamarckiens peuvent aussi éclairer une observation sur l'évolution qui a gagné en importance ces dernières années : l'évolution néo-lamarckienne était censée ajouter la possibilité pour les organismes de réagir rapidement quand des modifications de leur environnement rendaient leur forme de vie antérieure difficile ou impossible, c'est-à-dire que les orthocères étaient censés avoir échappé à l'extinction en réussissant dans leur lutte face à leur environnement à modifier leurs caractéristiques, se transformant par là en ammonites ou en nautiles.

Des travaux récents en théorie de l'évolution reviennent à cette idée et à l'éventualité qu'un concept connu aujourd'hui sous le nom de [*plasticité phénotypique*](https://www.sciencedirect.com/topics/agricultural-and-biological-sciences/phenotypic-plasticity) puisse jouer un rôle similaire. L'idée est qu'un organisme n'est pas nécessairement sélectionné pour une solution spécifique et permanente à un problème. À la place, dans certaines circonstances, il peut être préférable de garder de la flexibilité, surtout si l'environnement est très fluctuant. Ce n'est que plus tard, quand l'environnement changeant finit par se fixer, que la sélection naturelle entre en scène et rend plus permanentes certaines de ces options flexibles. Prendre plus au sérieux ce point délicat a attiré notre attention sur une série de phénomènes très intéressants reliant l'évolution, la biologie moléculaire et le développement des organismes. En ce sens, les néo-lamarckiens avaient raison : le changement environnemental est un problème réel et important que les organismes doivent pouvoir résoudre et la façon dont la sélection naturelle est parvenue à le résoudre met en jeu des questions théoriques intéressantes.

:::: think
**RÉFLEXION 9 :** On voit de temps à autre des exemples de ce type, où des théories qu'on croyait invalidées depuis longtemps reprennent vie quand elles sont réinterprétées à la lumière de données nouvelles. Qu'est-ce que cela nous dit sur la relation entre l'histoire des sciences et la pratique scientifique actuelle ? Faut-il encourager les scientifiques à mieux connaître l'histoire ou est-ce que ce serait une perte de temps ?
::::

Enfin, il mérite d'être dit que la recherche scientifique contemporaine nous a fait entrevoir des situations très particulières où, en fait, les caractères acquis pourraient effectivement être transmis à la descendance. Pour citer un seul exemple, des comportements au cours de la vie pourraient causer certains types de modifications de l'ADN, **autres** que des mutations de la séquence génétique elle-même (comme des modifications des [profils de méthylation](https://fr.wikipedia.org/wiki/M%C3%A9thylation#La_m%C3%A9thylation_de_l'ADN)), qui pourraient néanmoins être transmises et contrôler certains résultats à venir dans la vie des descendants. Bien qu'on ne puisse pas du tout réhabiliter l'intégralité de la théorie de Lamarck, il faudra peut-être bientôt actualiser les exemples cités dans ce cours !

:::: think
**RÉFLEXION 10 :** À votre avis, est-il utile de parler de résultats modernes de biologie moléculaire en utilisant des noms de théories anciennes comme « néo-lamarckisme » ? Donnez des raisons pour lesquelles ce rapprochement pourrait causer la confusion ainsi que des arguments en sa faveur.
::::


## RÉFLEXION : questions sur la nature des sciences

Qu'est-ce que l'histoire du néo-lamarckisme nous dit sur les aspects suivants des sciences ?

* pertinence des preuves (empirisme)
* rôle des convictions culturelles (identité nationale)
* réflexion interdisciplinaire
* rôle des probabilités dans l'inférence
* rôle de l'imagination et de la synthèse créative
* résolution des désaccords


## Lectures d'approfondissement

* Peter J. Bowler, [*The Eclipse of Darwinism: Anti-Darwinian Evolution Theories in the Decades around 1900*](https://www.worldcat.org/title/eclipse-of-darwinism-anti-darwinian-evolution-theories-in-the-decades-around-1900/oclc/26547189), Baltimore, Maryland, 1992, éd. Johns Hopkins University Press.
* Peter J. Bowler, « American Palaeontology and the Reception of Darwinism », *Studies in History and Philosophy of Biological and Biomedical Sciences*, 2017, vol. 66, p. 3-7, <https://doi.org/10.1016/j.shpsc.2017.06.002>.
* Mark Jaffe, [*The Gilded Dinosaur: The Fossil War between E. D. Cope and O. C. Marsh and the Rise of American Science*](https://www.worldcat.org/title/gilded-dinosaur-the-fossil-war-between-ed-cope-and-oc-marsh-and-the-rise-of-american-science/oclc/890171516), New York, 2000, éd. Crown.
* David Haig, « Weismann Rules! OK? Epigenetics and the Lamarckian Temptation », *Biology & Philosophy*, 2007, vol. 22, n^o^ 3, p. 415-428, <https://doi.org/10.1007/s10539-006-9033-y>.

#!/bin/bash

if [ "$1" = "--help" -o $# -ne 1 ]; then
  echo "copy.sh: copy these files to the website folder"
  echo ""
  echo "Usage: copy.sh <path to website folder>"
  exit 1
fi

DEST="$1"

for i in `seq 1 5`; do
  cp -v Output/Lesson$i.en.html "$DEST/content/lessons/lesson$i.html"
  cp -v Output/Lesson$i.fr.html "$DEST/content/cours/cours$i.html"
  cp -v $i-*/ImagesColor/* "$DEST/static/images/lessons/"
  cp -v Output/Lesson$i-*.pdf "$DEST/static/downloads/"
done

cp -v Output/Guide.en.html "$DEST/content/lessons/guide.html"
cp -v Output/Guide.fr.html "$DEST/content/cours/guide.html"
cp -v Output/Guide*.pdf "$DEST/static/downloads/"

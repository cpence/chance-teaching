# 1. Charles Darwin and the "Perfection" of Organisms

You probably all recognize this picture of [Charles Darwin](https://en.wikipedia.org/wiki/Charles_Darwin) (1809--1882), the bearded creator of the theory of evolution by natural selection. But Darwin wasn't always an old man. In 1837, when Darwin was just 28 years old, and a year after he returned home from [a five-year sailing trip around the world,](https://www.darwinproject.ac.uk/commentary/voyage-hms-beagle) he began to record early ideas about evolution in his private notebooks. This wasn't a popular position to take at the time. Only a few years earlier, an "evolutionary" book had been published anonymously (by, people later found out, a Scottish author named [Robert Chambers](https://en.wikipedia.org/wiki/Robert_Chambers_(publisher,_born_1802)) [1802--1871]), called [_Vestiges of the Natural History of Creation._](https://doi.org/10.5962/bhl.title.33033) It was mocked by everyone in the British scientific community. Darwin would later tell a friend that entertaining the thought that species might change over time was [a bit like confessing a murder!](https://www.darwinproject.ac.uk/letter/DCP-LETT-729.xml)

:::: think
**THINK[1]:** Why might arguing for the view that species have changed over time have been seen as controversial, unusual, or problematic? What kinds of scientific, religious, social, or cultural views could you see it as threatening?
::::

![Charles Darwin in 1869, photographed by famed photographer Julia Margaret Cameron (public domain; Wikimedia Commons)](darwin.jpg){.smallimg .left width=33%}

There were lots of reasons that people had thought that species probably didn't change. Some of them were religious. Many world religions have stories about the creation of life on earth, and in most of those traditions there's not an obvious place to make room for change in those creatures over time after they appeared. But those weren't the only reasons! Many were scientific. In this reading, we'll think about one of them: the *increase in complexity* of organisms.

It was a very old idea in biology, going back all the way to Aristotle in Ancient Greece, that all the living things on Earth could be placed roughly into [a single scale,](https://en.wikipedia.org/wiki/Great_chain_of_being) from the simplest things at the bottom (think pond scum, algae, or, after we discovered them, microorganisms or bacteria) all the way up to humans at the top. There was also often a racist element to this thinking: we could also sort humans by their level of "perfection," and since these theories were written by white people from Western Europe, white people from Western Europe were [the "most perfect" humans.](https://en.wikipedia.org/wiki/Polygenism)

:::: think
**THINK[2]:** Why would this ancient theory so easily support a racist world-view? How could you detect other instances of racist theories in science? More generally, what obligations do scientists have to ensure that their theories can't be used to support harmful social outcomes?
::::

Leaving aside the racism, it's easy enough to see how you can give what seems like a scientific basis to this kind of thinking. Many organisms appear to be more organized, more complex, more *interesting* than others. Think of the [extremely good eyesight of eagles,](https://www.audubon.org/news/how-golden-eagles-spot-prey-incredible-distances) the [running speed of a cheetah,](https://animals.howstuffworks.com/mammals/cheetah-speed.htm) or the [long-distance swimming ability of migrating whales,](https://phys.org/news/2015-04-migrating-whale-distance.html) and compare them to something like an earthworm, which just doesn't seem that complicated by comparison. One of Darwin's biggest challenges, then, was to convince the scientific community that this "scale of perfection" *is actually an illusion*. According to Darwin's theory, today's bacteria aren't "more evolved" or "less evolved" than we are -- they have simply evolved *differently,* following a separate path in their evolutionary history than we have.

While the bacteria and we humans have been going in different evolutionary directions for a very long time, Darwin argued that we do share a very remote common ancestor with those bacteria. Today, we can see lots of evidence for this, especially as regards the biochemical parts from which our cells and the cells of the bacteria are constructed, [which are more or less the same.](https://doi.org/10.1002/iub.146) For more than a billion years, the ancestors to today's humans and today's bacteria were getting better at fitting into dramatically different kinds of environments. We are one kind of end result, extremely good at surviving in some environments, and today's bacteria are another kind, extremely good at surviving in very, very different environments. Bacteria are much better at reproducing than us, for example, and there are species of bacteria that have adapted to nearly every environment imaginable, from the ability to live in [extremely cold water](https://www.astrobio.net/extreme-life/bacterial-survival-in-salty-antifreeze-raises-hope-for-life-on-mars-and-icy-moons/) to [boiling-hot volcanic vents at the bottom of the sea.](https://ocean.si.edu/ecosystems/deep-sea/microbes-keep-hydrothermal-vents-pumping)

:::: think
**THINK[3]:** In Darwin's day, there were two alternative ways to understand the difference between humans and bacteria. One would describe humans as "higher" than bacteria, on the basis of looking at their apparent complexity. The other would describe humans and bacteria as having evolved for the same amount of time, perhaps at different speeds (where humans evolved faster).

Do you think that this is only a conceptual difference, or could we collect data or perform experiments that might let us tell which of these explanations is right? If so, what would those data or experiments look like? More generally, how should we think about the relationship between conceptual change and experiment in science?
::::

Darwin thus had a task in front of him: he had to convince people that thinking of ourselves as "higher" organisms and bacteria as "lower" organisms was actually a mistake. As we will see, though, this is something that he himself struggled with. It's such a natural and obvious way to view the world around us that it's really hard -- even for someone as immersed in evolution as Darwin was -- to remember that our instinct to think of things like humans as evolutionarily "better" is fundamentally misleading.

\enlargethispage{\baselineskip}

In June of 1858, Darwin received from his fellow naturalist [Alfred Russel Wallace](https://en.wikipedia.org/wiki/Alfred_Russel_Wallace) (1823--1913) a paper about natural selection. While the two men actually had somewhat divergent ideas, Darwin was sure that concepts he had been developing for twenty years would be attributed to Wallace when he published his paper. Two of Darwin's friends arranged a July meeting of a major scientific association in London, the Linnean Society, where [Darwin and Wallace's papers were both presented,](http://darwin-online.org.uk/content/frameset?itemID=F350&viewtype=text&pageseq=1) along with a few old letters of Darwin's in which he spelled out natural selection, as a way of showing that Darwin was first. Neither Darwin nor Wallace was there, and we don't think either one was personally involved in setting it up -- but it did, in the end, succeed at giving Darwin the credit, and Wallace never contested this, even long after Darwin's death.

In any case, Darwin knew he needed to present his view to the world, and soon. He started writing it down, as quickly as he possibly could, in a book that would become [_The Origin of Species,_](https://doi.org/10.5962/bhl.title.68064) published in 1859.

:::: think
**THINK[4]:** Darwin was apparently very worried about what we now call being "scooped" -- someone else getting credit for a scientific idea that you were really the first person to think up. Why might this kind of prestige be important for scientists? Should scientists be motivated by this kind of social credit, or is it harmful to the scientific process? Is it relevant to the story that Darwin was wealthy and well-connected, while Wallace was middle-class and not part of the traditional "scientific establishment?"

**THINK[5]:** One thing that clearly helped Darwin here was the fact that he had important and influential friends who could quickly arrange a meeting at one of the most important scientific societies in the world. Major developments in science often involve not only the empirical or theoretical results, but also the social structures that you need in order to be able to distribute and publicize those results within the broader community. If Wallace had needed to do the same thing, without this kind of network, how could he have shared his results? What might you be able to do to bring attention to scientific results today that wouldn't have been possible for scientists in the nineteenth century? Are we better off now than we were then, or not?
::::

![Joseph Dalton Hooker, in 1860, in a lithograph by Rudolf Hoffmann (public domain; Wikimedia Commons); Campbell's Magnolia, illustrated by Hooker (CC-BY-SA 4.0; by Rawpixel at Wikimedia Commons)](hooker_magnolia.jpg){.medimg width=66%}

In the middle of writing his book, Darwin wrote a letter to his friend and colleague, the botanist [Joseph Dalton Hooker](https://en.wikipedia.org/wiki/Joseph_Dalton_Hooker) (1817--1911). In a previous letter, Hooker had summarized Darwin's argument as claiming that "the principle of selection tends to extermination of low forms & multiplication of high."[^hdletter] Obviously, Darwin wouldn't like this, and so he tried to explain his position to Hooker:

[^hdletter]: Hooker's letter to Darwin was sent on December 26, 1858; see <https://www.darwinproject.ac.uk/letter/DCP-LETT-2385.xml>
[^dhletter]: Darwin's letter to Hooker was sent on December 31, 1858; see <https://www.darwinproject.ac.uk/letter/DCP-LETT-2388.xml>

:::: {.quote title="Darwin, letter to Hooker (1858)"}
I do not think I said that I thought the productions of Asia were higher than those of Australia. I intend carefully to avoid this expression, for I do not think that any one has a definite idea what is meant by higher, except in classes which can loosely be compared with man. On our theory of Natural Selection, if the organisms of any area belonging to the Eocene or Secondary periods were put into competition with those now existing in the same area (or probably in any part of the world) they (i.e. the old ones) would be beaten hollow and be exterminated; if the theory be true, this must be so. In the same manner, I believe, a greater number of the productions of Asia, the largest territory in the world, would beat those of Australia, than conversely.[^dhletter]
::::

![One of the last Thylacines in captivity, Hobart Zoo, Australia, c. 1928 (public domain; Wikimedia Commons)](thylacine.jpg){.smallimg width=33%}

Darwin points here to an example that would have been familiar to his colleagues: the radical changes that had occurred in Australia since the arrival of European colonists. The [thylacine, also called the Tasmanian tiger](https://en.wikipedia.org/wiki/Thylacine) (not to be confused with the [Tasmanian devil,](https://en.wikipedia.org/wiki/Tasmanian_devil) which is both a cartoon character and an animal that still exists!) had already begun to disappear in Darwin's day; it was hunted aggressively in the nineteenth century and nearly extinct by the late 1920s. The [last living thylacine died in captivity in 1939.](https://www.youtube.com/watch?v=5RPap1BWYns) In large part, this and other extinctions were driven by the accidental or intentional introduction of European species (like cats, rabbits, foxes, and toads) into the country -- today there are numerous such invasive species in Australia, and managing their large populations costs billions of dollars a year. You can see why this enormous and rapid change would be important to scientists interested in understanding natural selection.

\enlargethispage{-\baselineskip}

:::: think
**THINK[6]:** The example of Australia points to the importance of connections between science and colonialism, especially throughout the nineteenth century. Why do you think that scientists might have been particularly interested in what was happening in the colonies? How might this exposure have changed our understanding of the world? How might it have been harmful to the people living in the colonies?

The same goes for military expansion. Darwin's trip around the world happened on the H.M.S. Beagle, a British navy ship in charge of surveying the coastline of South America to produce high-quality maps. What other connections can you think of between military power and scientific discovery? How might these links have altered the shape of the science that was produced?
::::

As Darwin goes on to write at the end of the quotation above, the case of invasive species in Australia seems to give us evidence that the organisms which evolve in larger areas are fitter -- better at surviving and reproducing -- than those which evolve in smaller places like Australia. As we just discussed, what matters is not organisms getting "better" or "higher" in any sense that we could recognize, but just that they would be able to thrive more than their competitors. Darwin would reinforce the point with one more example:

:::: {.quote title="Darwin, letter to Hooker (1858)"}
But this sort of highness (I wish I could invent some expression, and must try to do so) is different from highness in the common acceptation of the word. It might be connected with degradation of organisation: thus the blind degraded worm-like snake (*Typhlops*) might supplant the true earthworm. Here then would be degradation in the class, but certainly increase in the scale of organisation in the general inhabitants of the country. On the other hand, it would be quite as easy to believe that true earthworms might beat out the *Typhlops.* I do not see how this "competitive highness" can be tested in any way by us. And this is a comfort to me when mentally comparing the Silurian and Recent organisms. Not that I doubt a long course of "competitive highness" will ultimately make the organisation higher in every sense of the word; but it seems most difficult to test it.
::::

![A specimen of _Typhlops vermicularis_ in India (CC-BY-SA; by AshLin at Wikimedia Commons)](typhlops.jpg){.smallimg .left width=33%}

Whatever this evolutionary "highness" is must be different from what we call highness. Darwin points to the example of _Typhlops,_ a genus of reptiles often called "worm snakes." These snakes have adapted to live lives very similar to earthworms -- they have no eyes, and rarely spend time above ground. Of course, Darwin writes, these could spread all over the world and, in the end, take the place of earthworms. In that sense, Darwin writes, it seems like the suborder of snakes would be "doing worse," because there would be more of these strange, "degraded" snakes in the world than there were before. But a snake, even a very weird one, also seems like it is "more complicated" than an earthworm. So in that sense, a world with snakes and no earthworms is "higher" than ours. But how would we know whether this was "progress" or not in some kind of universal sense? How can we even form the question "comparing" the two cases?

\enlargethispage{-\baselineskip}

:::: think
**THINK[7]:** What is the problem that keeps us from being able to judge this global sense of "higher" and "lower," on Darwin's view? Is it just that we don't have access to enough data, or that our evidence is incomplete? Do we need a new theory to be able to understand it? Is it a problem with our concepts?
::::

This, then, is the position that Darwin argued for (most of the time). We should just stop using the terms "higher" and "lower," as there's no great sense of "progress" in evolution pointing from pond scum to people. As we've already seen, though, it's really hard to keep this idea in mind -- and it was hard even for Darwin himself! Let's look at a few examples where Darwin said something rather different. In the _Origin of Species_ itself (and remember that Darwin was writing this at the same time as he was writing the letter to Hooker we just read), he spent a lot of time thinking about a pretty obvious objection to his theory: if evolution *isn't* making organisms "higher" or "more perfect," then how are we supposed to understand some of the apparently "perfect" traits in nature, like the running speed of a cheetah, or, in Darwin's example here, the wings of a bird?

:::: {.quote title="Darwin, \emph{Origin} (1859)"}
When we see any structure highly perfected for any particular habit, as the wings of a bird for flight, we should bear in mind that animals displaying early transitional grades of the structure will seldom continue to exist to the present day, for they will have been supplanted by the very process of perfection through natural selection. Furthermore, we may conclude that transitional grades between structures fitted for very different habits of life will rarely have been developed at an early period in great numbers and under many subordinate forms.[^origin1]
::::

[^origin1]: Darwin, Charles. 1859. [_On the Origin of Species._](https://doi.org/10.5962/bhl.title.68064) 1st ed. London: John Murray, pp. 182--183.

![A flying fish, _Exocoetus volitans,_ illustrated by J.F. Hennig in 1801 (public domain; Wikimedia Commons)](flying_fish.jpg){.smallimg width=33%}

On observing something as complex as a bird wing, Darwin writes, we have to remember that -- and here things get confusing -- the earlier forms of that wing, the ones that didn't work as well, would have gone extinct, precisely because the new versions that we see today are simply *better* than the old ones. The "process of perfection through natural selection" would have produced newer, better wings, which in turn let organisms that have them do better than organisms that didn't.

We can see this more clearly if we think about organisms that *do* have bad imitations of birds' wings -- like flying fish! There are only a few species of flying fish, and all they can do is [occasionally glide through the air above the surface of a body of water.](https://www.nationalgeographic.com/animals/fish/group/flying-fish/) Darwin continues:

\enlargethispage{\baselineskip}

:::: {.quote title="Darwin, \emph{Origin} (1859)"}
Thus, to return to our imaginary illustration of the flying-fish, it does not seem probable that fishes capable of true flight would have been developed under many subordinate forms, for taking prey of many kinds in many ways, on the land and in the water, until their organs of flight had come to a high stage of perfection, so as to have given them a decided advantage over other animals in the battle for life. Hence the chance of discovering species with transitional grades of structure in a fossil condition will always be less, from their having existed in lesser numbers, than in the case of species with fully developed structures.[^origin2]
::::

[^origin2]: Darwin, p. 183.

The point Darwin is making goes something like this: because flying fish aren't very good at flying, they can't be very good at all of the *advantages* you get from flying. They can't catch bugs or evade predators in nearly the same kinds of ways that a real bird can. So they don't have any evolutionary reason to spread. Because there's not very many of them, we will be less likely to find them in the fossil record. That means it will always be hard for us to tell the story of how a highly perfected character trait, like bird wings, was developed in the history of life.

But wait! This is all wrong. We weren't supposed to be talking about higher and lower, better and worse, and we *definitely* weren't supposed to be talking about natural selection as a "process of perfection"! What happened? What is Darwin doing?

:::: think
**THINK[8]:** What should we do when we think we have found a case of confusion or contradiction in the works of a scientist? How might these kinds of contradictions cause problems for the scientific theories that the author was hoping to defend? On the other hand, how might this kind of contradiction or tension serve as a useful aid for the generation of new knowledge?
::::

What he's doing is making the same mistake that we already discussed at the beginning of the lesson, and one that's easy for many of us to make: confusing the ideas of *selective competition* and *evolutionary progress.* There's nothing about a bird wing that's "higher," or about a flying-fish wing that's "lower" -- *except* that bird wings work better at the kind of jobs that birds need them to do! Birds have a set of challenges to solve, which today we would call their [*ecological niche*:](https://www.sciencedirect.com/topics/earth-and-planetary-sciences/ecological-niche) things like what food they eat, what predators they need to avoid, where they sleep, and how they reproduce. Obviously those challenges are *very* different for a flying fish! Most birds wouldn't be very good at laying eggs in the water and swimming away from bigger fish.

:::: think
**THINK[9]:** If we wanted to compare the case of a bird's flight with that of a flying fish, what kinds of things would we need to keep in mind? What would you need to know about the lives of a flying fish and of a bird to make such a comparison? Do you think that the comparison even makes sense? What would the comparison help us learn about the structure of evolutionary theory?
::::

\enlargethispage{\baselineskip}

The moral of the story? We have to be *very* careful when we talk about evolutionary "perfection," or even about cases where it seems obvious to us that some feature is "better" than another one at some task. A little bit later in the _Origin,_ Darwin is more careful, as he responds again to the objection that something as complex as the eye could not have evolved by natural selection:

:::: {.quote title="Darwin, \emph{Origin} (1859)"}
To suppose that the eye, with all its inimitable contrivances for adjusting the focus to different distances, for admitting different amounts of light, and for the correction of spherical and chromatic aberration, could have been formed by natural selection, seems, I freely confess, absurd in the highest possible degree. Yet reason tells me, that if numerous gradations from a perfect and complex eye to one very imperfect and simple, each grade being useful to its possessor, can be shown to exist; if further, the eye does vary ever so slightly, and the variations be inherited, which is certainly the case; and if any variation or modification in the organ be ever useful to an animal under changing conditions of life, then the difficulty of believing that a perfect and complex eye could be formed by natural selection, though insuperable by our imagination, can hardly be considered real.[^origin3]
::::

[^origin3]: Darwin, pp. 186--187.

![Stages of eye complexity in mollusks (from Ayala, F. J. 2007. "Darwin's greatest discovery: Design without designer." _Proceedings of the National Academy of Sciences_ 104(suppl 1):8567--8573. <https://doi.org/10.1073/pnas.0701072104>. Copyright 2007 National Academy of Sciences.)](eye_evolution.jpg){.lgimg width=100%}

Even something that appears perfect to us can still have evolved by natural selection, as long as we understand how the series of steps that took us from the absence of an eye to a fully formed eye could have "made sense" from an evolutionary perspective -- that is, that each step would have given the organisms that had it some advantage over organisms that didn't. *This* is the sense of "higher" that we can and should use, and in fact we can demonstrate it in the case of the eye, by looking at other eyes that organisms have today! In the image just above, we can follow the way that an eye might develop, from just a simple spot that can detect light and shadow, to an extremely complicated eye like [that of the octopus.](https://gizmodo.com/octopus-eyes-are-crazier-than-we-imagined-1783195433) On the far left, the ability to detect light and shadow would be very useful if you were an organism living in shallow water and interested in that water's temperature. Turning that into a "pigment cup" starts to give you directional information about where the light is coming from. And then making more complex "cameras" lets you start to identify what you're actually looking at. At each step, organisms having that sort of eye can behave in a more complex manner, responding to their environment, food, and predators in ways that would let them succeed.

:::: think
**THINK[10]:** How is this concept both like and unlike our traditional understanding of "progress?" Many in the nineteenth century, when Darwin introduced evolution, were worried about the overall direction in which their culture was headed. What would they have thought about the impact of evolution on their view of the world and their place in it? How might this, in turn, have affected what kind of theory scientists like Darwin would have tried to develop?
::::

Evolutionary "highness," then, is about the ability to do better in the competition for survival and reproduction. Sometimes that might look like "progress" in our sense, when it lines up with things like running faster, seeing better, or being stronger. But it also might align with turning into an earthworm, or being a flying fish! Even Darwin had trouble carefully keeping those two ideas separate. At the very end of the _Origin of Species,_ he tried to reassure readers who might have found an evolutionary worldview scary. If there's no progress governing the future of life on earth, should we be afraid that somehow natural selection spells doom, a future for all of us of being earthworm-snakes? No, Darwin writes:

:::: {.quote title="Darwin, \emph{Origin} (1859)"}
As all the living forms of life are the lineal descendants of those which lived long before the Silurian epoch, we may feel certain that the ordinary succession by generation has never once been broken, and that no cataclysm has desolated the whole world. Hence we may look with some confidence to a secure future of equally inappreciable length. And as natural selection works solely by and for the good of each being, all corporeal and mental endowments will tend to progress towards perfection.[^origin4]
::::

[^origin4]: Darwin, p. 489.

But as we now know, this is Darwin being poetic, not being accurate. What Darwin would have meant by features "progressing toward perfection" -- what he had to mean as he painstakingly rid himself of ideas of "higher" and "lower," "better" and "worse," -- would not necessarily have looked much like what his nineteenth-century British readers would have thought of when they thought about "perfection."

:::: think
**THINK[11]:** Returning to one of the questions from early in the reading, why would a view like this possibly have been scary at the time -- that is, why would Darwin have wanted to reassure his readers? Do you think it's still troubling for readers today, or are we used to the idea of evolution? What kinds of relevant cultural and societal changes have taken place since that could be important to answering this question?

**THINK[12]:** After everything you have now read, do you think that we should talk about evolution in terms of progress, or not? As we have seen, there is still a sense in which evolution improves organisms, and it is undeniable that advanced features now exist that once did not. Is this enough to support a progressive understanding of evolution, or do you think that the arguments against progress are more compelling? What consequences would this have on the kind of science that you would do, if you were studying evolution professionally?
::::


## THINK: NOS Reflection Questions

What does Darwin's indecision about concepts of "higher" and "lower" tell us about the following features of the nature of science?

* nature of scientific credibility
* social responsibility of scientists
* role of cultural beliefs in science
* role of racial/class bias
* alternative explanations for phenomena
* scientific collaboration and competition


## Further Reading

* Bowler, Peter J. 1996. [_Charles Darwin: The Man and His Influence._](https://www.worldcat.org/title/charles-darwin-the-man-and-his-influence/oclc/731572144) Cambridge: Cambridge University Press.
* Ruse, Michael. 1996. [_Monad to Man: The Concept of Progress in Evolutionary Biology._](https://www.worldcat.org/title/monad-to-man-the-concept-of-progress-in-evolutionary-biology/oclc/756838729) Cambridge, MA: Harvard University Press.
* Moore, James. 2010. "Darwin's Progress and the Problem of Slavery." _Progress in Human Geography_ 34 (5): 555--82. <https://doi.org/10.1177/0309132510362932>.

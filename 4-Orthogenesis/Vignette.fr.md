---
lang: fr
---

# 4. Orthogénèse et « emballement évolutif »

Nous avons vu au cours précédent qu'une des questions cruciales pour beaucoup de scientifiques dans les temps qui ont suivi le développement de la théorie de Darwin était d'expliquer de manière adéquate les caractères non adaptatifs. Certains de ces caractères, présentés en séries semblant correspondre aux exigences de l'environnement des organismes, avaient donné des idées aux néo-lamarckiens. Un autre groupe d'exemples, sujet du cours d'aujourd'hui, a fourni les bases d'une autre explication non darwinienne du fonctionnement de l'évolution.

![August Weismann en 1915 (image : domaine public, Wikimedia Commons)](weismann.jpg){.smallimg width=33%}

Toutefois, avant de nous pencher sur cet exemple, revenons à quelqu'un dont nous avons parlé brièvement dans le cours précédent : [August Weismann](https://fr.wikipedia.org/wiki/August_Weismann) (1834-1914). Nous avons mentionné qu'il avait le premier avancé l'hypothèse que les cellules germinales étaient distinctes des cellules somatiques et les seules à participer à la transmission des caractères des parents à leur descendance. Pourtant, Weismann était tout aussi célèbre pour avoir proposé une [version de la sélection naturelle plus forte encore que celle de Darwin](https://www.lindahall.org/august-weismann/). Si, comme on l'a vu, Darwin disait certes que la sélection naturelle était très importante, il pensait que l'usage et le non-usage étaient parfois un facteur significatif. Il soulignait aussi le rôle d'autres processus, dont certains que nous évoquerons plus tard.

Weismann, lui, disait que Darwin n'était pas allé assez loin. Absolument tous les traits, selon Weismann, étaient le résultat de la sélection naturelle, des traits majeurs aux plus insignifiants en apparence. Il a écrit dans un essai en 1883 :

:::: {.quote title="Weismann, \emph{La vie et la mort} (1883)"}
Le monde si varié des animaux et des plantes qui nous entoure contient bien des éléments qu'on pourrait appeler nouveaux par comparaison avec les êtres primitifs hors desquels, cependant, tous ceux qui ont suivi se sont, selon notre théorie, développés par le processus de sélection.[^weism]
::::

[^weism]: August Weismann, [*Essais sur l'hérédité et la sélection naturelle*](https://doi.org/10.5962/bhl.title.28066), 1883. Traduction d'Henry de Varigny, Paris, 1892, éd. C. Reinwald, p. 94.

:::: think
**RÉFLEXION 1 :** Voyez-vous des caractères d'organismes qui soient problématiques pour cette position, des caractères qui ne semblent pas avoir été produits par sélection naturelle ? Que pourrait répondre Weismann à vos exemples ?
::::

Cette vision de la puissance de la sélection naturelle posait particulièrement problème aux biologistes qui avaient concentré leur attention sur l'importance des caractères apparemment **pas** adaptatifs du tout. Un de ces biologistes était l'Allemand [Theodor Eimer](https://fr.wikipedia.org/wiki/Theodor_Gustav_Heinrich_Eimer) (1843-1898). Les publications d'Eimer, extrêmement populaires en Allemagne dans les premiers temps après la publication de *L'origine*, furent diffusées dans le monde un peu plus tard car le néo-lamarckisme et d'autres mécanismes non darwiniens d'évolution étaient plus largement acceptés.

La théorie d'Eimer prit le nom d'**orthogénèse**. Voyons comment il l'a décrite puis analysons ce qu'il entendait par là.

:::: {.quote title="Eimer, \emph{Sur l'orthogénèse} (1898)"}
L'évolution dans une direction déterminée, ou orthogénèse, est une loi universellement valable. Elle réfute de façon définitive l'assertion de Weismann sur l'omnipotence de la sélection naturelle, qui n'est qu'une exagération du darwinisme et implique implicitement l'autre thèse soutenue par Weismann inconditionnellement jusqu'ici, d'ailleurs jadis défendue également par Darwin, \[thèse qui avance\] que tous les caractères existant chez les animaux ont une certaine utilité. L'orthogénèse montre que les organismes se développent dans des directions déterminées, sans aucune considération pour l'utilité, en raison de causes purement physiologiques, du fait de la *croissance organique*, selon le terme que j'utilise pour ce processus. Aucun caractère absolument préjudiciable ne pourrait, vu la nature de l'affaire, continuer à exister mais la sélection naturelle, dont Weismann présume qu'elle est le seul facteur déterminant dans la transformation, ne pourrait non plus avoir aucune action, excepté dans le cas d'un caractère préexistant sur lequel, parce qu'il était déjà utile, la sélection naturelle pourrait avoir prise et ainsi faire son office.[^eimer1]
::::

[^eimer1]: Gustav Theodor Eimer, [*On Orthogenesis: And the Impotence of Natural Selection in Species-Formation*](https://doi.org/10.5962/bhl.title.87978), traduction anglaise de Thomas J. McCormack, 1898, Chicago, éd. Open Court, p. 2. Traduction de l'anglais : Sandra Mouton.

L'orthogénèse selon Eimer est donc une théorie disant que l'évolution s'engage souvent dans ce qu'il appelle des « directions déterminées ». Elle se trouve bloquée, canalisée sur une voie particulière. Qu'une évolution dans cette direction soit bénéfique pour l'organisme ou non, elle continue. Ceci est en partie dû, d'après Eimer, au fonctionnement de la variation. Avec l'orthogénèse, on aurait un plus petit nombre de variations disponibles, de telle sorte que se tromper de route, aller contre le mouvement de la « direction déterminée » déjà établie au sein du groupe en évolution, serait impossible.

:::: think
**RÉFLEXION 2 :** Imaginez qu'on vous ait demandé en 1898 d'évaluer la théorie d'Eimer. À quelles expériences ou observations pourriez-vous penser pour tester certaines de ses affirmations ? (Imaginez que vous ayez accès au registre entier des fossiles et la possibilité de faire se reproduire n'importe quels organismes dans toutes les conditions imaginables.)
::::

Si tout ceci a l'air différent de la sélection naturelle, c'est parce que ça l'est ; Eimer n'a pas mâché ses mots à propos de la théorie de Darwin :

:::: {.quote title="Eimer, \emph{Sur l'orthogénèse} (1898)"}
Le fait que les variations des êtres vivants suivent, en conformité parfaite avec la loi, quelques directions déterminées et ne se produisent pas accidentellement dans les directions les plus variées ou dans toutes les directions possibles suffit à faire voler en éclats les fondements de la doctrine darwinienne. En effet, la doctrine de Darwin doit toujours avoir à sa disposition un assortiment de variations très divers pour que la sélection puisse jouer un rôle déterminant dans la production des formes. Et parce qu'elle est un présupposé nécessaire de la doctrine, cette présomption de la présence constante de tous les caractères possibles est toujours affirmée comme un fait par les défenseurs d'une sélection naturelle toute-puissante.

Si au contraire un nombre restreint de tendances déterminées d'évolution prédomine, alors celles-ci façonnent le monde organique et il ne reste à la sélection qu'une tâche très secondaire.[^eimer2]
::::

Pour Eimer, la sélection naturelle ne peut tout simplement pas avoir fait ce que lui attribue Darwin : en particulier, parce que l'orthogénèse limite les types de variations mises à disposition de la sélection, le processus de sélection naturelle n'a plus la capacité de créer librement des adaptations. Au contraire, ce processus est condamné à agir dans les seules directions dont l'orthogénèse lui a laissé l'accès, que ces directions soient ou non ce que nous appellerions adaptatives.

:::: think
**RÉFLEXION 3 :** On dit souvent de la sélection naturelle qu'elle **crée** des adaptations. Si on veut comprendre l'objection d'Eimer, selon laquelle elle ne peut pas, nous devons d'abord réfléchir à ce que cela signifie. D'après vous, que veut-on dire par « création d'adaptations » ? Comment la sélection naturelle et la variation agissent-elles ensemble pour fabriquer de nouvelles caractéristiques des organismes ?
::::

Même dans ces conditions, Eimer ne pensait pas que la sélection naturelle était totalement inutile :

:::: {.quote title="Eimer, \emph{Sur l'orthogénèse} (1898)"}
Je dois, du reste, répéter encore et encore que la sélection naturelle ne produit en aucun cas quoi que ce soit de nouveau. Elle ne peut agir que sur du matériau déjà existant et elle ne peut même pas l'utiliser avant qu'il n'ait atteint un certain perfectionnement, avant qu'il ne soit déjà utile. La sélection peut uniquement éliminer ce qui est tout à fait préjudiciable et préserver ce qui est utile. En sélectionnant toujours l'utile, elle renforce son développement mais les faits prouvent que même cela ne peut se produire que dans une mesure restreinte.[^eimer2]
::::

[^eimer2]: Eimer, *On Orthogenesis: And the Impotence of Natural Selection \[...\]*, p. 21.

Dans la vision d'Eimer, la sélection naturelle aurait encore un rôle mais ce serait uniquement de finir par éliminer tout organisme qui s'est développé dans des directions **nuisibles**. Ici, la sélection naturelle agit seulement en éliminant les organismes qui ne fonctionnent pas, plutôt qu'en créant l'adaptation.

![Un fossile de mégalocéros (image : CC-BY-SA, Atirador, Wikimedia Commons)](irish_elk.jpg){.medimg .left width=66%}

Tout cela sera plus facile avec un exemple et autant en choisir un spectaculaire : le mégalocéros. Il est parfois appelé « élan irlandais », bien que ce ne soit pas un élan et qu'il n'ait pas uniquement vécu en Irlande, ou « cerf géant ». Son principal signe distinctif est assez évident : il a des bois géants, qui pouvaient atteindre 40 kg et 3,5 m d'envergure.

On connaît cette espèce depuis très longtemps : les premiers spécimens ont été décrits au XVII^e^ siècle. Évidemment, aucun animal semblable n'existait plus à l'époque (en tout cas en Europe occidentale, où les naturalistes avaient catalogué de manière assez approfondie les espèces existantes) et au XIX^e^ siècle, le mégalocéros était donc décrit comme un exemple célèbre d'extinction. Tout cela laisse aux scientifiques de la théorie de l'évolution quelques questions évidentes : qu'est-ce qui a causé l'extinction de ces cervidés ? Et d'abord, comment l'évolution a-t-elle pu créer un être aux caractéristiques si exagérées ? Quelle fonction ces bois pouvaient-ils bien remplir ?

:::: think
**RÉFLEXION 4 :** Quelles utilisations un mégalocéros pouvait-il faire de ces bois géants ? Quelle autre origine de leur apparition voyez-vous, à part la sélection naturelle ? Comment feriez-vous la comparaison entre les diverses explications auxquelles vous avez pensé ?
::::

Il faut noter que presque tout le monde dans ce débat part du principe que les bois ne sont **pas** une adaptation, c'est-à-dire qu'il n'aurait pas été utile à la survie des organismes d'avoir des bois aussi grands. La taille de leurs bois faisait courir le risque aux mégalocéros de se casser le cou (dans un combat ou par accident) et même s'ils n'étaient pas mortellement dangereux, les bois exigeaient un énorme investissement dans les tissus osseux et musculaires du cou qui aurait pu être alloué différemment, par exemple en rendant l'animal plus résistant aux prédateurs.

Si les bois n'étaient pas une adaptation, qu'est-ce que Darwin aurait pu en dire ? Darwin avait trois approches principales pour analyser l'apparition de caractères qui ne semblaient pas adaptatifs. Nous en avons vu une au cours précédent : il pensait que dans certains cas du moins, le fait qu'un organisme utilisait beaucoup un caractère pouvait influer sur sa transmission à ses descendants. On ne peut pas l'appliquer ici, cependant : que voudrait dire l'idée qu'un mégalocéros en particulier utilisait beaucoup ses bois ?

La deuxième explication de Darwin concernait ce qu'il appelait la **sélection sexuelle**. Pour avoir une descendance nombreuse, il faut survivre et surpasser les membres de la même espèce mais aussi réussir à trouver un partenaire de reproduction. L'exemple le plus fréquemment employé par Darwin pour ce processus était justement les bois de cervidé. Si les mâles doivent se battre pour pouvoir se reproduire, posséder des armes ou des défenses pour ces combats serait, selon Darwin, un avantage évident, même s'il pourrait globalement rendre la vie plus difficile à l'animal dans d'autres aspects de son existence.

:::: think
**RÉFLEXION 5 :** On a critiqué la façon de penser de Darwin concernant la sélection sexuelle en l'accusant de n'être qu'un condensé des rôles sexuels du XIX^e^ siècle, avec des mâles violents et agressifs qui se battent pour les femelles timides aux goûts difficiles. Est-ce une position raisonnable ou une transposition injustifiée de valeurs de société dans le domaine scientifique ? Serait-il possible d'avoir une théorie de la sélection sexuelle qui ne rencontre pas ces problèmes et si oui, comment ? Plus généralement, comment pouvons-nous veiller à ne pas simplement interpréter nos données scientifiques à la lumière de ce qui semble raisonnable ou acceptable dans notre société ?
::::

Mais d'autres scientifiques ont objecté que même l'explication de la sélection sexuelle semble problématique, en tout cas pour un caractère aussi extrême que les bois des mégalocéros. Quelle serait exactement leur utilité, disaient ces scientifiques ? Un énorme poids tout au bout d'un cou relativement fragile ne serait pas très utile pour les combats. Les bois optimisés pour le combat ont tendance à être soit légers et pointus (comme [les bois de cerf](https://www.youtube.com/watch?v=dg4VeesS6_I)) soit lourds mais assez compacts ([comme chez le bélier](https://www.youtube.com/watch?v=Ez7RUSCUhzk)). Ces bois de mégalocéros ont les inconvénients des deux systèmes : ils ne sont bons ni pour le combat ni pour la défense.

Darwin disposait d'encore une autre explication à mobiliser, qu'il appelait la **corrélation de croissance**. Il écrit que dans bien des cas, on ne sait pas pourquoi mais il semblerait que certaines caractéristiques des organismes soit corrélées ou liées pendant la croissance de l'organisme. À mesure qu'une partie du corps grossit, une autre la suit et grossit aussi ou une autre encore rapetisse car il « n'y a plus la place » pour elle. Il serait alors possible de comprendre la grande taille des bois du mégalocéros de cette façon.

Une autre caractéristique importante du mégalocéros est la grande taille de l'animal. Il faisait partie des plus gros animaux dans la famille des cervidés vivant sur la Terre à cette époque. Peut-être la sélection naturelle encourageait-elle ce trait ? Avoir une grande taille est, bien sûr, une excellente stratégie pour éviter la prédation. Si être de plus en plus gros entraînait une augmentation disproportionnée de la taille des bois, cela pouvait être l'origine de ce qu'on constate chez le mégalocéros.

:::: think
**RÉFLEXION 6 :** Voyez-vous des problèmes avec cette explication ? Si vous étiez l'adversaire de Darwin, comment critiqueriez-vous cet argument ?
::::

Mais Eimer avait une réplique imparable. Pourquoi la sélection naturelle n'avait-elle pas pu briser la corrélation, s'il était aussi important d'avoir un gros cerf avec des bois plus petits ? Les darwiniens ne trouvèrent pas de réponse. Comme nous l'avons vu, parce que Darwin n'avait aucune idée de la génétique et des bases fondamentales de la constitution des caractères des organismes (nous y reviendrons au cours suivant), il ne savait pas **comment** fonctionnait effectivement la corrélation de croissance. Il savait seulement qu'elle semblait se produire dans de nombreux cas d'élevage agricole et de variations observables en milieu sauvage.

:::: think
**RÉFLEXION 7 :** Quelle quantité de preuves, et de quels types, faut-il avoir pour étayer une théorie scientifique ? L'orthogénèse d'Eimer comme la corrélation de croissance de Darwin font appel à des processus que nous ne comprenons pas entièrement et les deux positions pensent disposer d'éléments empiriques confirmant ces processus. Comment argumenter en faveur de la corrélation de croissance ou de l'orthogénèse sans autres données que les **effets** du processus ?
::::

L'orthogénèse, en revanche, était capable d'offrir une explication claire de la croissance exagérée des bois : le pauvre mégalocéros était resté coincé dans une impasse. La force d'orthogénèse l'avait engagé dans la direction d'une augmentation de la taille des bois et la variation par conséquent à la disposition de cet organisme était uniquement dans le sens de bois plus grands. À ce stade, l'espèce était condamnée ! Elle ne pouvait que développer des bois de plus en plus grands jusqu'à aboutir à l'extinction.

Il est important de comprendre à quel point cette explication pouvait séduire. Eimer passait le plus clair de son temps à rechercher des exemples d'orthogénèse comme explications des organismes vivants, pas uniquement des fossiles (il avait étudié les papillons de manière approfondie, par exemple), et le côté « carré » des explications que la canalisation de variation pouvait offrir pour les tendances non adaptatives dans la variation était difficile à égaler pour beaucoup de philosophes.

Pourtant, à mesure que se développait la génétique, on trouva peu de preuves que la variation était effectivement limitée de cette manière. L'hypothèse de l'orthogénèse s'écroula dans la plupart des domaines de la biologie, bien qu'elle ait duré un peu plus longtemps chez les paléontologues qui s'intéressaient moins aux mécanismes de la variation elle-même et avaient moins de possibilité de les étudier. On pouvait expliquer la plupart des caractéristiques du monde biologique sans avoir à ajouter la composante supplémentaire de l'orthogénèse.

:::: think
**RÉFLEXION 8 :** On entend souvent des références à la simplicité d'une hypothèse scientifique, aussi appelée « principe du rasoir d'Occam », d'après le philosophe médiéval Guillaume d'Occam (ou William of Ockham) (1285-1347). Les hypothèses scientifiques plus simples sont ainsi considérées comme de meilleurs reflets du monde. Est-ce d'après vous un bon principe de raisonnement scientifique ? À quels problèmes peut-il conduire et comment les éviter ?
::::

Et notre mégalocéros, alors ? Une version modifiée de la thèse de Darwin de la corrélation de croissance a émergé avec les avancées contemporaines de la biologie. La [théorie contemporaine de l'**allométrie**](https://fr.wikipedia.org/wiki/Allom%C3%A9trie) étudie précisément ces corrélations et permet de considérer que ces parties corrélées pourraient se développer à des rythmes différents. Comme le célèbre biologiste Stephen Jay Gould (1941-2002) l'a expliqué dans les années 1970 :

:::: {.quote title="Gould, de la revue \emph{Evolution} (1974)"}
Le \[mégalocéros\] est un cerf géant de la fin du Pléistocène qui s'étendait au sud jusqu'en Afrique du nord et à l'est jusqu'en Chine. Depuis sa première description scientifique en 1697, il a joué un rôle majeur dans les débats sur l'histoire de la vie. Cuvier l'a utilisé pour prouver le phénomène d'extinction et fixer la base d'une échelle de temps géologique. Plus tard, le mégalocéros est devenu l'étendard des anti-darwiniens : ils invoquaient l'orthogénèse pour réfuter la sélection naturelle et attribuaient l'extinction à une tendance non adaptative à des bois immenses. Les bois posaient un problème grave à la Synthèse moderne : on les expliquait en général par une corrélation allométrique de l'augmentation de la taille des bois à celle, avantageuse, de la taille du corps, ce qui compensait les problèmes de bois reconnus comme disproportionnés. Virtuellement tous les manuels en théorie de l'évolution citent le mégalocéros comme un cas de réfutation de l'orthogénèse par l'allométrie : pourtant, personne n'a jamais généré de données quantitatives sur le sujet.[^gould1]
::::

[^gould1]: Stephen Jay Gould, « The Origin and Function of "Bizarre" Structures: Antler Size and Skull Size in the "Irish Elk," *Megaloceros Giganteus* », *Evolution*, 1974, vol. 28, n^o^ 2, p. 191-220, <https://doi.org/10.1111/j.1558-5646.1974.tb00740.x>, p. 216. Traduction de l'anglais : Sandra Mouton.

On voit que dans les années 1970, la plupart des biologistes pensaient que l'hypothèse de l'allométrie ou une théorie semblable se révèlerait être l'explication de l'évolution des bois du mégalocéros. Mais on ne disposait toujours pas de données solides dessus. Il nous faut démontrer clairement, à partir de données empiriques, que ce type de corrélation existe effectivement **et** démontrer que les bois eux-mêmes n'étaient pas sélectionnés, c'est-à-dire que c'était vraiment la sélection de la grande taille du corps qui était à l'œuvre, et seulement elle.

:::: think
**RÉFLEXION 9 :** Gould envisage implicitement les trois possibilités suivantes pour expliquer ce changement évolutif. Tout d'abord, il se pourrait que les bois aient évolué indépendamment de la taille de l'animal, c'est-à-dire que bois et taille de l'animal ne soient en fait pas corrélés du tout. Mais s'ils le sont, il y a deux autres explications possibles. Si taille de l'animal et taille des bois augmentaient toutes deux, il se pourrait que la sélection naturelle ait été responsable dans les deux cas. Ou bien, dernière possibilité, il se pourrait que la sélection naturelle soit **uniquement** responsable de l'augmentation de la taille de l'animal et que celle de la taille des bois soit entièrement « accidentelle ».

Quel type de données collecteriez-vous pour pouvoir trancher entre ces trois hypothèses ? Comment sauriez-vous que vous en avez prouvé une et réfuté les autres ?
::::

Gould a commencé par s'atteler à faire les mesures et il a effectivement constaté une corrélation positive. Mais il a aussi fait remarquer qu'il n'y a pas de raison immédiate de penser que la sélection naturelle ne peut pas avoir créé ces caractères également :

:::: {.quote title="Gould, de la revue \emph{Evolution} (1974)"}
J'ai mesuré 79 crânes et bois de mégalocéros pour répondre à deux questions \[\...\] Si la sélection a préservé les cerfs de grande taille, il s'ensuivrait logiquement des bois relativement grands en raison de la corrélation.

Pourtant, l'allométrie positive avérée n'entraîne pas nécessairement l'interprétation habituelle : la grande taille des animaux peut être une conséquence de bois avantageux ou les bois et les grands animaux peuvent avoir été sélectionnés de concert. La préconception que les bois sont disproportionnés est basée sur la notion *a priori* que les bois servent nécessairement d'armes pendant les combats. \[\...\] Mais les cervidés utilisent souvent leurs bois pour établir une position dominante et obtenir l'accès aux femelles par des combats simulés ou ritualisés \[\...\] chez le mégalocéros, seul dans ce cas parmi les cervidés à bois palmés, on voyait l'ensemble de la palme quand il regardait simplement droit devant lui.

Les immenses bois du mégalocéros étaient avantageux en eux-mêmes. Son extinction peut être reliée à des changements glaciaires tardifs du climat.[^gould2]
::::

[^gould2]: Gould, « The Origin and Function of "Bizarre" Structures \[...\] », p. 217.

![Un paon, extrêmement peu furtif devant les prédateurs (image : CC-BY-SA, BS Thurner Hof, Wikimedia Commons)](peacock.jpg){.smallimg width=33%}

Gould veut que nous nous demandions pourquoi nous avons présupposé que ces bois ne pouvaient pas être adaptatifs. Souvenez-vous que quand nous avons évoqué l'utilité de ces bois, nous avons d'abord supposé qu'ils serviraient pour se battre. Et comme ils faisaient de mauvaises armes d'attaque et de mauvaises protections, nous avons conclu qu'ils ne pouvaient pas avoir été des adaptations. Mais ils pouvaient avoir d'autres utilités.

Un autre exemple de sélection sexuelle, utilisé également par Darwin, est le plumage des paons. Les plumes de la queue du paon mâle le rendent beaucoup plus facile à attraper pour les prédateurs mais si les exhiber le rend plus attirant aux yeux des paonnes, le jeu peut en valoir la chandelle. Peut-être que les bois du mégalocéros faisaient de bonnes décorations, pour séduire des partenaires potentielles. Ou peut-être que le combat n'en est pas un et que c'est en fait un rituel où les cerfs s'affrontent dans de fausses batailles, où l'animal qui a les plus petits bois, parce qu'il sait déjà qu'il finira par perdre, admet sa défaite au bout d'un moment.

\enlargethispage{\baselineskip}

Ces deux scénarios semblent donner des explications très plausibles qui ne font appel qu'à la sélection naturelle. Et dans ce cas, l'extinction des mégalocéros n'exige pas d'interprétation spéciale : c'est un banal changement climatique, quand les glaciers qui couvraient la majeure partie de leur territoire se sont retirés, qui a causé leur extinction.

Donc, de la fin du XIX^e^ siècle au milieu du vingtième, ont existé des explications très diverses sur le comment et le pourquoi de l'évolution des bois du cerf géant :

* l'orthogénèse, soit une limitation des variations possibles, pointant dans la direction des grands bois
* la corrélation de croissance ou allométrie, soit la sélection opérant sur une partie du corps mais influant sur une autre
* la sélection sexuelle, soit l'utilisation des bois pour attirer des partenaires et dissuader les rivaux.

Décider entre elles n'était pas facile et dépendait des informations disponibles à l'époque. L'orthogénèse constituait une réaction très naturelle face à des tendances comme celle-ci, constatées dans les fossiles et chez les espèces vivantes, et ce n'est que lorsque les preuves en sa faveur ont commencé à entrer en contradiction avec le début de la génétique qu'elle a été abandonnée.

Mais prenons un temps de recul pour réfléchir à une autre question. Pourquoi ce problème était-il si important pour autant de biologistes ? Qu'est-ce qui dans les caractères **non adaptatifs** était si fascinant qu'un grand nombre de biologistes (comme on l'a vu dans ce cours et dans le précédent) voulait absolument que des théories comme le néo-lamarckisme ou l'orthogénèse les expliquent ? Comme nous l'avons déjà vu, l'impact du travail de Weismann offre un élément de réponse : si quelqu'un avance très explicitement que **tous** les traits sont le résultat de la sélection naturelle, se pencher sur les caractères non adaptatifs est une façon évidente d'y répondre.

Un autre facteur, relevé par beaucoup de philosophes et scientifiques, est lié aux conséquences **pour la société** de la théorie de l'évolution[^bowler]. Même à l'époque d'Eimer, on commençait à lire Darwin comme un plaidoyer en faveur de la [« survie du plus apte » dans l'élaboration des politiques publiques](https://eugenicsarchive.ca/discover/connections/535eee377095aa0000000259), appuyant l'idée qu'on devrait laisser mourir les « faibles » afin que cela profite aux « forts ». Ce n'était pas la position de Darwin lui-même mais faire appel à une force comme l'orthogénèse, qui avançait que les espèces pouvaient changer sans qu'il y ait de lutte ou de question d'aptitude, faisait une place définitive à une approche moins violente du changement évolutif.

[^bowler]: Peter J. Bowler, « Theodor Eimer and Orthogenesis: Evolution by "Definitely Directed Variation" », *Journal of the History of Medicine and Allied Sciences*, 1979, vol. XXXIV, n^o^ 1, p. 40-73, <https://doi.org/10.1093/jhmas/XXXIV.1.40>.

Une partie de ces scientifiques et philosophes, y compris Eimer, avaient même des partis pris philosophiques qui les conduisaient à penser que le monde **devrait** présenter des comportements ordonnés, directionnels, comme ceux proposés par l'orthogénèse plutôt que la complexité ramifiée en mille branches qu'impliquait la théorie de l'évolution.

\enlargethispage{-\baselineskip}

:::: think
**RÉFLEXION 10 :** D'après vous, la théorie scientifique a-t-elle une importance pour déterminer les politiques sociales ou gouvernementales ? Autrement dit, quels types de découvertes scientifiques pourraient être importants pour la façon dont nous organisons la vie au sein de la société ? Quel usage abusif pourrait-on faire de la science quand celle-ci entre sur le terrain politique ? Quelles sont les responsabilités des scientifiques qui pensent que leur travail pourrait être détourné de cette manière ?
::::

On voit donc qu'étudier l'orthogénèse est l'outil parfait pour comprendre certaines des préoccupations des biologistes de la fin du XIX^e^ siècle et du début du vingtième. Et ces préoccupations ne sont pas exclusivement scientifiques : certaines l'étaient quand d'autres incluaient aussi des positions philosophiques et sociales. Les controverses scientifiques ont souvent à voir avec bien plus que la science et le débat sur les variations non adaptatives ne fait pas exception.


## RÉFLEXION : questions sur la nature des sciences

Qu'est-ce que l'histoire de l'orthogénèse nous dit sur les aspects suivants des sciences ?

* alternatives d'explication
* pertinence des preuves (empirisme)
* responsabilité des scientifiques vis à vis de la société
* changement conceptuel
* rôle des préjugés sur les sexes
* incertitude
* rôle de l'étude systématique (par opposition à l'anecdote)


## Lectures d'approfondissement

* Peter J. Bowler, [*The Eclipse of Darwinism: Anti-Darwinian Evolution Theories in the Decades around 1900*](https://www.worldcat.org/title/eclipse-of-darwinism-anti-darwinian-evolution-theories-in-the-decades-around-1900/oclc/26547189), Baltimore, Maryland, 1992, éd. Johns Hopkins University Press.
* Peter J. Bowler, « Theodor Eimer and Orthogenesis: Evolution by "Definitely Directed Variation" », *Journal of the History of Medicine and Allied Sciences*, 1979, vol. XXXIV, n^o^ 1, p. 40-73, <https://doi.org/10.1093/jhmas/XXXIV.1.40>.
* Mark A. Ulett, « Making the Case for Orthogenesis: The Popularization of Definitely Directed Evolution (1890--1926) », *Studies in History and Philosophy of Biological and Biomedical Sciences*, 2014, vol. 45, p. 124-132, <https://doi.org/10.1016/j.shpsc.2013.11.009>.

from glob import glob
from os import path
import subprocess
import platform

if platform.system() == "Windows":
    pathsep = ";"
else:
    pathsep = ":"

env = Environment()

languages = ["en", "fr"]
papers = ["A4", "Letter"]
colors = ["BW", "Color"]

targets = []
docx_targets = []


def build_pdf(out_pdf, src_md, lang, paper, color, toc=False):
    basedir = path.dirname(src_md)

    if toc:
        toc_string = "--toc --toc-depth=1"
    else:
        toc_string = ""

    return [
        Mkdir("Output"),
        (
            "pandoc -t latex %s -o %s --standalone "
            "%s "
            "--lua-filter ./build/filter.lua "
            "-V fontsize=12pt "
            "-V indent=false "
            "-H %s.tex -H %s.tex -H %s.tex -H Common.tex "
            "--resource-path ./build/%s./%s/Images%s/"
        )
        % (src_md, out_pdf, toc_string, lang, paper, color, pathsep, basedir, color),
    ]


def build_guide_html(out_html, src_md, lang):
    out_basename = path.basename(out_html)
    lang_tag = "lessons"
    if lang == "fr":
        lang_tag = "cours"

    return [
        Mkdir("Output"),
        Mkdir("tmp"),
        # Initial HTML conversion, using the guide template and adding a TOC
        (
            "pandoc -t html %s -o tmp/content.html "
            "--lua-filter ./build/filter.lua "
            "--mathjax --shift-heading-level-by=1 "
            "--toc --toc-depth=2 "
            "--standalone "
            "--template=./build/guide-template.html"
        )
        % (src_md),
        # Take the Markdown header block from the MD file and add it to the
        # HTML output
        "grep '^---$$' -B 9999 %s > tmp/%s" % (src_md, out_basename),
        "echo '' >> tmp/%s" % (out_basename),
        "echo '{{<%s>}}' >> tmp/%s" % (lang_tag, out_basename),
        # Take off the footnotes section from the bottom of the HTML
        (
            "head -n "
            "-`grep -A 9999 '<section class=\"footnotes\"' tmp/content.html "
            "| wc -l` tmp/content.html >> tmp/%s"
        )
        % (out_basename),
        # Move it to the output
        Move(out_html, "tmp/%s" % (out_basename)),
        Delete("tmp"),
    ]


def build_lesson_html(out_html, src_md, lang):
    out_basename = path.basename(out_html)
    lang_tag = "lessons"
    if lang == "fr":
        lang_tag = "cours"

    return [
        Mkdir("Output"),
        Mkdir("tmp"),
        # Initial HTML conversion
        ("pandoc -t html %s -o tmp/%s " "--lua-filter ./build/filter.lua --mathjax")
        % (src_md, out_basename),
        # Convert the first H1 at the top to the title
        (
            'sed -i \'s@<h1.*">\(.*\)</h1>@+++\\ntitle = "\\1"\\n+++\\n\\n{{<%s>}}@\' '
            "tmp/%s"
        )
        % (lang_tag, out_basename),
        # Move it to the output
        Move(out_html, "tmp/%s" % (out_basename)),
        Delete("tmp"),
    ]


def build_docx(out_docx, src_md):
    return [
        Mkdir("Output"),
        ("pandoc -t docx %s -o %s " "--lua-filter ./build/filter.lua --standalone")
        % (src_md, out_docx),
    ]


for lang in languages:
    guide_md = "Guide/Guide.%s.md" % (lang)

    for paper in papers:
        # Only build the guide PDF in color, but for each paper size
        guide_pdf = "Output/Guide-%s.%s.pdf" % (paper, lang)
        env.Command(
            guide_pdf,
            guide_md,
            build_pdf(guide_pdf, guide_md, lang, paper, "Color", True),
        )
        targets.append(guide_pdf)

    # Build the guide HTML
    guide_html = "Output/Guide.%s.html" % (lang)
    env.Command(guide_html, guide_md, build_guide_html(guide_html, guide_md, lang))
    targets.append(guide_html)

    # Optionally build guide DOCX
    guide_docx = "Output/Guide.%s.docx" % (lang)
    env.Command(guide_docx, guide_md, build_docx(guide_docx, guide_md))
    docx_targets.append(guide_docx)

    lessons = glob("*/Vignette.%s.md" % (lang))

    for lesson_md in lessons:
        lesson_dir = path.dirname(lesson_md)

        # Build the lesson HTMLs
        lesson_html = "Output/Lesson%s.%s.html" % (lesson_dir[0], lang)
        env.Command(
            lesson_html, lesson_md, build_lesson_html(lesson_html, lesson_md, lang)
        )
        targets.append(lesson_html)

        # Optionally build lesson DOCXs
        lesson_docx = "Output/Lesson%s.%s.docx" % (lesson_dir[0], lang)
        env.Command(lesson_docx, lesson_md, build_docx(lesson_docx, lesson_md))
        docx_targets.append(lesson_docx)

        # Build the lesson PDFs in color and BW
        for paper in papers:
            for color in colors:
                lesson_pdf = "Output/Lesson%s-%s-%s.%s.pdf" % (
                    lesson_dir[0],
                    color,
                    paper,
                    lang,
                )

                env.Command(
                    lesson_pdf,
                    lesson_md,
                    build_pdf(lesson_pdf, lesson_md, lang, paper, color),
                )
                # Make sure to rebuild the PDFs if the images change
                env.Depends(lesson_pdf, "%s/Images%s" % (lesson_dir, color))

                targets.append(lesson_pdf)

# Build everything by default
env.Default(targets)

# Friendly alias for DOCX generation
env.Alias("docx", docx_targets)

# 2. Darwin, Sir John Herschel, and Scientific Reasoning

![Isaac Newton, engraving from a painting by Kneller (public domain; Wikimedia Commons)](newton.jpg){.smallimg .left width=33%}

One thing that people immediately noticed after Darwin published his theory was that it simply *looked different* than other kinds of scientific theories of his day. In the nineteenth century in Britain, there was a fairly rigid idea of what "doing science" was supposed to look like. It was modeled on physics, mostly from a combination of the ideas of [Francis Bacon](https://en.wikipedia.org/wiki/Francis_Bacon) (1561--1626) and [Isaac Newton](https://en.wikipedia.org/wiki/Isaac_Newton) (1643--1727). On this view, we were supposed to go out and do as many measurements of the natural world as we possibly could, gathering as much data as we could find to perform an *induction* -- creating a general hypothesis about what's going on in the world by reasoning from all of this data we've collected.

Just to take one example of what this might look like, imagine that we didn't know what the law was that governed the trajectory of things in free fall. We should, this view argued, go out and drop as many things as we can, watch them fall, and record their trajectories. We would then be able to examine this large collection of data and try to understand what kind of mathematical description would generate it -- and if we were very good, we might produce something like Newton's second law as applied to freely falling bodies, $y(t) = y_0 + v_0 t - \frac{1}{2} gt^2$. All of the data that we had collected would serve as the evidence that we had found the right answer.[^newtonind]

[^newtonind]: Notably, this isn't necessarily how Newton himself would have described his own work -- but it is how he was often presented by philosophers in the 19th century.

:::: think
**THINK[1]:** Does this match your idea of "the scientific method?" Can you think of elements of scientific practice that might be missing from this account, or fields of science whose work couldn't be described in this way? More broadly, do you think that all parts of science use one, single "scientific method," or not?
::::

This wasn't really what Darwin was doing. He did gather lots of data, but it wasn't really data *about* evolution actually happening out there in the world. Darwin collected a lot of information about the kind of variations that occurred not in the wild, but in the opposite of the wild: in agricultural contexts where people had been trying to [create new kinds of plants or animals with particular characteristics.](https://en.wikipedia.org/wiki/British_Agricultural_Revolution) He also knew a lot about the ways in which species were related to one another. From this, he reasoned, as we saw in the last reading, that the arrangement of species that we see in the world around us could be perfectly explained if species were related to one another by *descent with modification* -- if related species seem related precisely because they have both descended from a common ancestor. He then argued that the mechanism that was actually responsible for this change in species was *natural selection* -- that organisms would adapt and diversify as a result of the competition to survive and reproduce.

:::: think
**THINK[2]:** How would data that we acquired from agricultural breeding be relevant to answering these kinds of questions? How is it similar and different from data that we would collect from natural organisms? What kinds of problems might you run into if you tried to use data derived in one context to support a theory derived in a very different context?
::::

Darwin's argument for these two claims involves all kinds of components that don't look like Newton. He builds an analogy between what natural selection is doing and what agricultural breeders are doing. He makes an argument, which we now call [*inference to the best explanation* or *abduction,*](https://plato.stanford.edu/entries/abduction/) that understanding evolution in his sense would give us an easy way to explain a huge array of phenomena in the natural world, things like [embryology,](https://en.wikipedia.org/wiki/Recapitulation_theory) the [distribution of species on islands like the Galapagos,](https://www.jstor.org/stable/40305871) or the way that species cluster into a [series of smaller groups within bigger groups](https://en.wikipedia.org/wiki/Taxonomic_rank) (the taxonomic hierarchy, from species to kingdoms).

All of these are good arguments -- we'll talk more about them in a little while -- but they didn't look right to many of Darwin's peers. In this lesson, we'll spend some time thinking about what their understanding of science was, why Darwin's theory didn't fit it, and why and how we have expanded our understanding of "good science" since Darwin's day.

In part of his work on physics, Newton famously wrote down [a number of "rules for philosophizing"](https://plato.stanford.edu/entries/newton-philosophy/) (we'd call them rules for building scientific theories). The first of these was that

:::: {.quote title="Newton, \emph{Principia}"}
No more causes of natural things should be admitted than are both true and sufficient to explain their phenomena.[^principia]
::::

[^principia]: Newton, Isaac. 2016 [1726]. \emph{The Principia: The Authoritative Translation and Guide.} Trans. I. Bernard Cohen, Anne Whitman, and Julia Budenz. Berkeley, CA: Univ. of California Press.

In other words, if you don't understand how something works, you aren't allowed to just invent some kind of new process out there in the world that would explain it, but for which we don't have any evidence at all. If I don't know what's causing the bacteria in my lab to die, I'm not allowed to just say that magic fairies are responsible, unless I can go out and find more independent evidence for magic fairies. With the same idea in mind, Newton bragged elsewhere: "I don't make hypotheses." No wild guesses, only real causes.

:::: think
**THINK[3]:** Do you think it would be better to be too strict with our standards for scientific investigation, or more relaxed? What kinds of problems might arise in each case, and how could we balance or possibly correct them?
::::

\enlargethispage{\baselineskip}

This view worked fine for a while -- until scientists began to want to deal with things that *didn't* look like Newton's "real causes." In the nineteenth century, we started to get some [really good evidence that light was a wave.](https://www.youtube.com/watch?v=y9c8oZ49pFc) There's just one problem: you can't see these waves directly, and we don't really understand how they work. Worst of all, we don't know what they're waves *in* -- how can you have a wave without something like water to be moving around when the wave passes? It seems like this was just the kind of thing that Newton didn't want us to do.

In response, a number of philosophers tried to appease the ghost of Newton by coming up with criteria for what would count as a "real cause." One of the most famous was [Sir John Herschel](https://en.wikipedia.org/wiki/John_Herschel) (1792--1871). Herschel was an amazing scientist: he is now best known as an astronomer, who gave names to the moons of the planets Saturn and Uranus (his father, William Herschel, had discovered Uranus), but he also was a pioneer in early photography, named a number of different new species of plants, and [even invented blueprints.](http://www.alternativephotography.com/cyanotype-history-john-herschels-invention/)

:::: think
**THINK[4]:** We don't see very many scientists today that are famous for doing so many different kinds of things. The idea of being a "polymath," someone who is good at many different fields of knowledge, has steadily become less possible. What reasons can you think of that might be driving this kind of disciplinary specialization? Is it a good thing that scientists today are more specialized than they were a century ago? What would be the advantages and disadvantages?
::::

More importantly for us, he wrote a book, published in 1830, titled _A Preliminary Discourse on the Study of Natural Philosophy._[^pd] This was a volume designed to lay out what he thought were "good methods" for making scientific observations and building scientific theories. ("Natural philosophy" is an old name for what we'd today call "science.") In it, he argued for what he thought Newton had meant by "real cause."[^vc] One thing that makes science so useful, Herschel argues, is that it gives us lots of them:

[^pd]: Herschel, John F. W. 1830. [_A Preliminary Discourse on the Study of Natural Philosophy._](https://doi.org/10.5962/bhl.title.19835) 1st ed. London: Longman, Rees, Orme, Brown, & Green.
[^vc]: If you want to read more about this debate, you should know that the scientists, historians, and philosophers who have talked about it, including Herschel, usually use Newton's original Latin phrase, *vera causa,* instead of talking about "real causes."

\enlargethispage{-\baselineskip}

:::: {.quote title="Herschel, \emph{Preliminary Discourse} (1830)"}
Experience having shown us the manner in which one phenomenon depends on another in a great variety of cases, we find ourselves provided, as science extends, with a continually increasing stock of such antecedent phenomena, or causes (meaning at present merely proximate causes), competent, under different modifications, to the production of a great multitude of effects, besides those which originally led to a knowledge of them. To such causes Newton has applied the term [real cause].[^pd138]
::::

[^pd138]: Herschel 1830, section 138.

What does Herschel mean? We make scientific discoveries in particular, narrow contexts -- for instance, Newton discovered gravity in trying to understand the orbits of the planets and the moon, and many early theories of electricity were discovered as people [experimented with rudimentary batteries.](https://en.wikipedia.org/wiki/Leyden_jar) But then we often find out that, because these are real causes, they explain all kinds of other stuff besides. Newton's theory explains much more than just the orbit of the moon, and electricity is good for a lot more than [zapping your friends as a party trick.](https://www.atlasobscura.com/articles/shocking-scenes-from-benjamin-franklins-experimental-electricity-parties)

:::: think
**THINK[5]:** When we propose a new scientific theory, we will have some set of evidence that we already know, which has led us to think about that theory in the first place. After we propose it, we will discover new evidence, things that we didn't already know about before the theory was proposed. This difference between old evidence and new evidence has often been taken by philosophers and scientists to be very important to understanding whether a theory might in fact be accurate.

What do you think are the important differences between old evidence and new evidence? Why might you say that new evidence would be better than old, or vice versa? Is there a difference between a theory's being "consistent with" old evidence, and its "explaining" new evidence?
::::

Next, what should we do if we think we have discovered a new real cause? Say that we're Darwin, and we think that natural selection is one. What's the next step?

:::: {.quote title="Herschel, \emph{Preliminary Discourse} (1830)"}
Whenever, therefore, we think we have been led by induction to the knowledge of the proximate cause of a phenomenon...our next business is to examine deliberately and seriatim all the cases we have collected of [the phenomenon's] occurrence, in order to satisfy ourselves that they are explicable by our cause.[^pd172]
::::

[^pd172]: Herschel 1830, section 172.

Getting around Herschel's archaic language, he's telling us here that we need to go out into the world and examine every case that we know of that involves our new cause, and really make sure that they could be explained in the way that we thought. We would need, if we were Darwin, to go look at every species-change we could, and describe how that species-change might have resulted from natural selection.

:::: think
**THINK[6]:** One important difference between Herschel and Darwin is simply a question of the fields in which each worked: Herschel is primarily a physicist or engineer, and Darwin is a life scientist. Can you think of reasons that we might need different standards for what counts as "good science" in biology versus in physics? What about in the social sciences? Do these standards mean that some of these fields are "more scientific" than others, or not?
::::

And even then, we aren't done. How do we know that we didn't just invent a really clever, but ultimately false hypothesis that just *happens* to explain the things we've seen so far? The history of science is filled with [examples of people doing exactly this.](https://en.wikipedia.org/wiki/Phlogiston_theory) To prevent it, we have to keep working:

:::: {.quote title="Herschel, \emph{Preliminary Discourse} (1830)"}
A law of nature has not that degree of generality which fits it for a stepping-stone to greater inductions, unless it be universal in its application.... Our next step in the verification of an induction must therefore consist in extending its application to cases not originally contemplated: in studiously varying the circumstances under which our causes act, with a view to ascertain whether their effect is general; and in pushing the application of our laws to extreme cases.[^pd176]
::::

[^pd176]: Herschel 1830, section 176.

We have to go look for new cases -- things that we didn't even know about when we wrote down our theory -- and make sure that they, too, don't cause us any problems. This includes looking for similar examples to the ones that we had already found, in slightly different conditions, and also seeking bizarre, extreme examples, places where our new cause might be likely to break, and making sure that it still works there, too.

It should start to be clear, then, why Herschel wouldn't have liked Darwin's science. There was just no way, given the biological knowledge available at the time (or even available now!) to go look at *every single* change in the history of life, and think about how to describe it in terms of natural selection. And there was no way in Darwin's day to *go produce* new cases of natural selection to make sure that it worked right. (This we can now do, in examples like the famous [Long-Term Evolution Experiment.](https://en.wikipedia.org/wiki/E._coli_long-term_evolution_experiment))

:::: think
**THINK[7]:** Another distinction that might help us explain the difference between Herschel and Darwin's views of science is the contrast between science which proceeds mainly by performing controlled experiments, and science that primarily involves observation of phenomena in the world around us. What kinds of differences in data or theory might arise as a result? What kinds of scientific procedure might be possible in one case but not possible in the other?

A similar kind of claim in the philosophy of science is sometimes described as the importance of "learning by doing" or "learning by making." Do we know more about a scientific system when we can create one ourselves, or can we learn all there is to know by observing systems "in the wild?"
::::

And Herschel made sure that Darwin knew that he didn't like his theory. Despite the fact that Darwin highly respected Herschel, and the fact that the _Origin_ seems to adhere to the pattern that Herschel told us to follow, Herschel added a long footnote to a book that he published a few years after Darwin's _Origin_ explaining why he disliked it so much:

:::: {.quote title="Herschel, \emph{Physical Geography} (1861)"}
...we can no more accept the principle of arbitrary and casual variation and natural selection as a sufficient account, *per se,* of the past and present organic world, than we can receive the Laputan method of composing books (pushed *a l'outrance*) as a sufficient one of Shakspeare and the _Principia._[^geognote]
::::

[^geognote]: Herschel, John F. W. 1861. [_Physical Geography: From the Encyclopaedia Britannica._](https://archive.org/details/physicalgeograph00hers) Edinburgh: Adam and Charles Black, section 11, note.

For those of you who haven't read _Gulliver's Travels,_ in that book the hero visits an island called Laputa, where people create books by using a machine that puts together random strings of words, reading them after they are finished to see whether or not there is anything intelligible inside. Darwin's evolutionary theory, Herschel thinks, is like this. Herschel continued:

:::: {.quote title="Herschel, \emph{Physical Geography} (1861)"}
Equally in either case, an intelligence, guided by a purpose, must be continually *in action* to bias the directions of the steps of change -- to regulate their amount -- to limit their divergence -- and to continue them in a definite course. We do not believe that Mr. Darwin means to deny the necessity of such intelligent direction. But [intelligent direction] does not, so far as we can see, enter into the formula of his law, and without it we are unable to conceive how the law can have led to the results.
::::

There are two things Darwin could add to his theory in order to make it acceptable to Herschel. One would just be "intelligent direction" -- that is, divine intervention -- which would modify the course of evolutionary change, as it could see just which changes would be good for an organism and which would be problematic.

:::: {.quote title="Herschel, \emph{Physical Geography} (1861)"}
On the other hand, we do not mean to deny that such intelligence may act according to a law (that is to say, on a pre-conceived and definite plan). Such a law, stated in words, would be no other than the actual observed law of organic succession; or one more general, taking that form when applied to our own planet, and including all the links of the chain which have disappeared. But the one law is a necessary supplement to the other, and ought, in all logical propriety, to form a part of its enunciation. Granting this, and with some demur as to the genesis of man, we are far from disposed to repudiate the view taken of this mysterious subject in Mr. Darwin's work.
::::

The other thing that Darwin could add would be a "law of variation," by which evolution would operate -- in much the same way as planets revolve according to the law of gravity. (This law, Herschel would argue, was instituted by divine intervention.) But this law would require exactly what we said above Darwin couldn't do: the "actual observed law of organic succession," or, put more simply, the exact steps in variation that the natural world in fact underwent. Darwin, of course, could not know this, and neither could we, so Herschel rejects evolution outright.

:::: think
**THINK[8]:** How might you respond to these criticisms, if you were Darwin? Does he need to make changes in his theory in order to account for Herschel's view?
::::

![Sir John Herschel, photographed by famed photographer Julia Margaret Cameron (public domain; Wikimedia Commons)](herschel.jpg){.smallimg width=33%}

Before we analyze Herschel's criticism in more detail and try to understand where he went wrong, let's look at one more source. As it turns out, we have access to Herschel's own copy of _Origin of Species,_ in a library in Texas. This volume contains the hand-written notes that Herschel took while he read Darwin's work.[^marginalia]

[^marginalia]: You can read all of these marginalia online, as they are public domain: <https://doi.org/10.5281/zenodo.3974296>

In chapter 5 of the _Origin,_ Darwin is writing about what it is that causes variations -- why do offspring not look exactly like their parents? This variation is crucial to evolutionary theory, since we won't see production of new adaptations if we never see the right kinds of variations. But Darwin doesn't have our understanding of genetics, much less our understanding of DNA, so in fact he didn't know how variations came about. All he could say was that it seemed clear that they *were* generated (as made clear by our success in agriculture, for instance).

But again, this wasn't good enough for Herschel. After this chapter of the _Origin,_ Herschel wrote in his copy:

:::: {.quote title="Herschel's copy of Darwin (1859)"}
D. recognizes an unknown cause of slight individual differences -- but claims for 'natural selection' the character of a 'sufficient theory' in regard to the results of those differences.
::::

On the same page, Herschel has underlined a few of Darwin's claims in the last paragraph of the chapter. He draws our attention to a comparison between two things Darwin has said. First, he underlines the sentence where Darwin argues that "*a cause for each* [variation] *must exist*," but then double underlines the claim that "[natural selection] **gives rise** to all the more important modifications of structure."

What is it, Herschel seems to ask, that is really behind the appearance of adaptation? Is it the causes of variations that are important for creating new species, or natural selection? And if the answer is "both," then how can a theory of natural selection work without a theory of variation to go with it?

:::: think
**THINK[9]:** We might rephrase this part of the disagreement between Herschel and Darwin as follows. Darwin believes that, because there is currently no theory that describes how variation works in the wild, he can treat the way that variations appear as a "black box." We have enough evidence, he thinks, to believe that the black box works, even if we don't know how. Herschel, on the contrary, thinks that we have to open up the black box, and provide an explanation for what's going on inside, for how variations are actually created.

How do you think we could resolve this dispute between the two? More generally, if we have a debate between scientists not over the empirical facts of the matter, but over what kinds of standards we should accept for a scientific explanation, to what kinds of resources should we turn to solve it? What do you think would be a "good argument" from either Herschel or Darwin for their view? Who should get to decide what those standards for explanations are?
::::

Let's step back and look at Herschel's general response to Darwin's theory. For Herschel, there are two ways in which changes in the world might operate -- they might be divinely directed, or they might operate according to laws of nature, which give rise to his "real causes." Natural selection *could,* he thinks, be a real cause. But to make that argument, we would have to demonstrate two things that Darwin couldn't in 1859. First, we'd need to show that many of the changes that happened in a species could be explained as a result of natural selection. And second, we'd need to understand the law of variations, so that we could be sure that the variation needed to produce the new organism would really be available in the right place at the right time.

We can use Herschel's critiques to look at a number of important points about evolution. First, Herschel is exactly right that there *is* something missing from Darwin's theory. For the first fifty years after Darwin published the _Origin of Species,_ scientists struggled to understand what variation is, how it works, and how it could be compatible with the structure of life and evolutionary change. Before we began to understand the genetic basis of evolution, biologists from 1859 until the 1920s worked on a variety of ways around the problem, from theories of evolution that didn't really include much discussion of the way that offspring inherited their character traits from parents, to speculative theories about how variation might work based on the limited cellular biology that was available at the time. Even if Herschel is wrong that biology needed to be governed by a "law of variation," he was right to see that our understanding of evolution would be much more powerful if we could connect it with knowledge of how variations came to be and were inherited.

:::: think
**THINK[10]:** What exactly do you think we would get by adding genetics to evolution? One answer might be that we would understand what was happening at a "lower level" (that is, the biochemistry) as a way to explain what was going on at the "higher level" (that is, organisms). There is a powerful trend toward this sort of *reductionism* in the sciences -- the idea that explanations that work at lower levels are "better" than those that work at higher levels.

Do you think this kind of reductionism is correct, or not? What advantages and disadvantages are produced by an explanation in terms of chemistry, as opposed to a biological explanation? What might this mean for how we think about the relationships between scientific fields?
::::

![A pair of peppered moths, one each of the light and dark form (CC-BY-SA; by Siga at Wikimedia Commons)](moths.jpg){.smallimg .left width=33%}

Second, Herschel is right that it is hard -- sometimes *very* hard -- to tell stories about how natural selection might have given rise to some particular feature of an organism. Occasionally we can get lucky, and things are simpler -- like in the famous example of the [peppered moths whose color changed when the trees on which they sat became darkened with pollution.](https://doi.org/10.1038/hdy.2012.92) But often things are more difficult, and we certainly need to be careful. Sometimes, we can get much more detail, especially today, when we can [connect an ecological, evolutionary story with its molecular and biochemical fundamentals,](https://www.youtube.com/watch?v=5UkxNkuc_OY) something that was of course impossible for Darwin.

Finally, it's true that evolutionary theory doesn't work the way that Herschel thought a scientific theory should be built. It requires us to use different kinds of arguments to support our position. But unlike Herschel, we don't have to say that these are automatically inferior. Inference to the best explanation, for instance, is a kind of argument we use all the time in our daily lives. Imagine the way in which a detective story usually goes. The detective thinks about all of the possible ways in which a murder might have happened, and then goes around to collect as much evidence as possible, hoping that the evidence will show us that one of those explanations is clearly better than all of the others. Of course, the detective isn't running any experiments, and they aren't collecting data like Herschel might have wanted. But when they develop a theory about who is responsible for committing the murder, they're often correct!

:::: think
**THINK[11]:** The difference here between *kinds* of arguments is an old one in the study of philosophy. Some arguments, like in mathematics or logic, are *deductive* -- if their premises are true, their conclusions *have* to be true. Some, like Herschel's approach to science, are *inductive* -- we move from a large collection of data to a general theory about that data. Some, finally, are *abductive* -- we support some explanation of our phenomena because it is much more compelling than the alternatives.

Can you think of examples of arguments in science that use each of these three kinds of argument? Do some of them seem more powerful than others? Some will be available and unavailable in different fields at different times, depending on the evidence we have. Does this correspond to a judgment about the overall "quality" of those sciences?
::::

In short, arguments in science -- especially when we leave physics and begin to study biological science or social science -- are extremely diverse, and it's no easy feat for us to understand how they all connect together. Evolutionary science is complex and difficult, and we should be skeptical of anyone who tells us that all scientific investigations have to follow one single methodology.


## THINK: NOS Reflection Questions

What does the dispute between Herschel and Darwin tell us about the following features of the nature of science?

* completeness of evidence
* robustness (agreement among different types of data)
* evidential relevance (empiricism)
* consilience with established evidence
* role of analogy, interdisciplinary thinking
* forms of persuasion
* response to criticism


## Further Reading

* Secord, James A. 2014. "The Conduct of Everyday Life: John Herschel's Preliminary Discourse on the Study of Natural Philosophy." In [_Visions of Science: Books and Readers at the Dawn of the Victorian Age,_](https://www.worldcat.org/title/visions-of-science-books-and-readers-at-the-dawn-of-the-victorian-age/oclc/923634059) 80--106. Chicago: University of Chicago Press.
* Cannon, W. F. 1961. "John Herschel and the Idea of Science." _Journal of the History of Ideas_ 22 (2): 215--39. <https://doi.org/10.2307/2707834>.
* Hodge, M. J. S. 1992. "Darwin's Argument in the Origin." _Philosophy of Science_ 59 (3): 461--4. <https://doi.org/10.1086/289682>.

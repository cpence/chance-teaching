---
lang: fr
---

# 2. Darwin, John Herschel et le raisonnement scientifique

![Isaac Newton, gravure à partir d'un tableau de Kneller (image : domaine public, Wikimedia Commons)](newton.jpg){.smallimg .left width=33%}

Une chose sauta immédiatement aux yeux du public quand Darwin publia sa théorie : elle ne **ressemblait pas** aux autres théories de son époque. Dans la Grande Bretagne du XIX^e^ siècle régnait une conception assez rigide de ce à quoi « faire des sciences » était censé ressembler : le modèle était la physique et essentiellement un mélange des idées de [Francis Bacon](https://fr.wikipedia.org/wiki/Francis_Bacon_(philosophe)) (1561-1626) et d'[Isaac Newton](https://fr.wikipedia.org/wiki/Isaac_Newton) (1643-1727). Dans cette optique, les scientifiques devaient arpenter le monde naturel pour y faire autant de mesures que possible, rassembler autant de données que possible, afin de procéder par **induction**, c'est-à-dire d'élaborer une hypothèse générale sur ce qui se passe dans le monde en raisonnant à partir de toutes ces données collectées.

Pour donner une illustration, imaginez qu'on ne sache pas la loi gouvernant la trajectoire des objets en chute libre. Selon cette conception de la science, on doit prendre autant d'objets qu'on peut, les faire tomber, observer leur chute et enregistrer leur trajectoire. On est alors en position d'examiner cette vaste collection de données pour essayer de comprendre quelle expression mathématique pourrait les générer. Si on est très fort, on arrive à quelque chose comme le résultat de la deuxième loi de Newton appliquée à un corps en chute libre, $z(t) = z_0 + v_0 t - \frac{1}{2} gt^2$. Toutes les données collectées serviront de preuve qu'on est arrivé à la bonne réponse.[^newtonind]

[^newtonind]: Il est intéressant de noter que ce n'est pas nécessairement la façon dont Newton lui-même aurait décrit son travail. C'est toutefois comme ça qu'il était souvent présenté par les philosophes du XIX^e^ siècle.

:::: think
**RÉFLEXION 1 :** Est-ce l'idée que vous vous faites de la « méthode scientifique » ? Voyez-vous des aspects de la pratique scientifique que cette description oublie ou des domaines scientifiques qui ne fonctionnent pas de cette manière ? Plus largement, pensez-vous que tous les champs des sciences utilisent une seule et unique « méthode scientifique » ?
::::

Darwin ne procédait pas vraiment comme ça. Il rassemblait effectivement beaucoup de données mais il ne s'agissait pas de données **sur** l'évolution en action dans le monde naturel. Il a collecté des montagnes d'informations sur les types de variations présentes non pas en milieu sauvage mais dans son opposé, c'est-à-dire les contextes agricoles de [création de nouvelles plantes ou animaux aux caractéristiques particulières](https://en.wikipedia.org/wiki/British_Agricultural_Revolution). Darwin était aussi très bien informé sur les liens entre espèces. Comme on l'a vu dans le cours précédent, il en a tiré la conclusion que l'organisation des espèces que nous voyons dans le monde qui nous entoure s'expliquerait parfaitement si on posait que les espèces étaient liées entre elles par la **descendance avec modification**, c'est-à-dire si les espèces liées semblaient apparentées précisément parce qu'elles descendaient d'un ancêtre commun. Il a ensuite avancé que le mécanisme responsable de ces changements dans les espèces était la **sélection naturelle**, c'est-à-dire que les organismes s'adaptaient et se diversifiaient à la suite de la compétition pour survivre et se reproduire.

:::: think
**RÉFLEXION 2 :** Les données collectées dans le contexte des pratiques agricoles ont-elles une pertinence pour des questions de ce type ? Quels sont les points communs et les différences avec des données collectées sur des organismes sauvages ? Quels types de problèmes risque-t-on de rencontrer si on essaie d'utiliser des données tirées d'un contexte pour étayer une théorie élaborée dans un autre contexte, très différent ?
::::

En faveur de ces deux thèses, Darwin déploie de nombreux arguments ne ressemblant pas à la méthode Newton. Il fait une analogie entre l'action de la sélection naturelle et celle des agriculteurs. Il utilise un raisonnement, que nous appelons aujourd'hui [**inférence à la meilleure explication** ou **abduction**](https://plato.stanford.edu/entries/abduction/), selon lequel l'évolution telle qu'il l'entend nous fournit une explication facile pour un très grand éventail de phénomènes du monde naturel, notamment [l'embryologie](https://fr.wikipedia.org/wiki/Th%C3%A9orie_de_la_r%C3%A9capitulation), la [distribution des espèces sur les îles comme les Galapagos](https://www.jstor.org/stable/40305871) ou le regroupement des espèces en une [série de sous-groupes au sein de groupes plus grands](https://fr.wikipedia.org/wiki/Rang_taxonomique) (la hiérarchie taxonomique, de l'espèce au règne).

Tous ces arguments sont valides (nous en reparlerons un peu plus loin) mais ils faisaient tiquer beaucoup de scientifiques du temps de Darwin. Dans ce cours, nous nous pencherons sur leur vision de la science, sur pourquoi la théorie de Darwin ne rentrait pas dans ce cadre et sur la vision plus large de la « science de qualité » que nous avons construite depuis l'époque de Darwin, comment et pourquoi.

Une partie du travail de Newton sur la physique comprend les célèbres [« règles de philosophie »](https://plato.stanford.edu/entries/newton-philosophy/) qu'il a édictées (on les appellerait aujourd'hui des règles d'élaboration de théories scientifiques). La première est celle-ci :

:::: {.quote title="Newton, \emph{Principes mathématiques}"}
Il ne faut admettre de causes, que celles qui sont à la fois vraies et suffisantes pour expliquer les phénomènes[^principia].
::::

[^principia]: Isaac Newton, *Principes mathématiques de la philosophie naturelle*, 1687, traduction adaptée de celle d'Émilie du Châtelet (Paris, 1759, éd. Desaint & Saillant).

En d'autres termes, si on ne comprend pas comment quelque chose fonctionne, on n'a pas le droit d'inventer un nouveau processus à l'œuvre dans la nature, qui l'expliquerait mais dont on n'a aucune preuve. Si je ne sais pas ce qui fait mourir les bactéries à mon labo, je n'ai pas le droit de me contenter d'accuser un lutin magicien, sauf si je suis en mesure de trouver d'autres preuves, indépendantes, de l'existence des lutins magiciens. Dans le même esprit, Newton se vantait ailleurs : « je ne fais pas d'hypothèses ». Pas de devinettes, uniquement les causes véritables.

:::: think
**RÉFLEXION 3 :** Pensez-vous qu'il vaut mieux être trop strict dans nos exigences de rigueur en recherche scientifique ? Ou plus souple ? Quels types de problèmes pourraient découler de chacune de ces options et comment pourrait-on les compenser et les corriger ?
::::

Cette attitude a fonctionné pendant un certain temps, jusqu'à ce que les scientifiques décident de se préoccuper de ce qu'on ne pouvait **pas** classer avec les « causes vraies » de Newton. Ainsi, au XIX^e^ siècle, on a commencé à découvrir des éléments vraiment [convaincants sur le fait que la lumière était une onde](https://www.youtube.com/watch?v=y9c8oZ49pFc). Il y avait pourtant un petit problème : il était impossible de voir ces ondes directement et on ne comprenait pas vraiment comment elles fonctionnaient. Pire, on ne savait pas **dans quoi** ces ondes ondulaient : comment peut-on avoir une onde sans un milieu comme l'eau qui bouge au passage de l'onde ? Cela ressemblait fort au type d'activité que Newton désapprouvait.

En réponse, des philosophes ont essayé de réconcilier la théorie avec l'esprit de ce grand homme en définissant des critères de ce qui pouvait être considéré comme une « vraie cause ». Un des plus célèbres était [John Herschel](https://fr.wikipedia.org/wiki/John_Herschel) (1792-1871). C'était un scientifique extraordinaire : il est aujourd'hui plus connu comme l'astronome qui a donné un nom aux satellites des planètes Saturne et Uranus (son père, William Herschel, avait découvert Uranus) mais c'était aussi un pionnier de la photographie, il a nommé un certain nombre de nouvelles espèces de plantes et a [même inventé le procédé d'imprimerie par cyanotypie.](http://www.alternativephotography.com/cyanotype-history-john-herschels-invention/)

:::: think
**RÉFLEXION 4 :** On ne voit pas beaucoup de scientifiques de nos jours qui soient célèbres pour autant de choses différentes. Être un esprit universel, polymathe, quelqu'un de fort dans de nombreux domaines de savoir différents, est devenu moins possible avec le temps. Quelles explications pouvez-vous trouver à cette spécialisation ? Est-ce une bonne chose que les scientifiques d'aujourd'hui se spécialisent plus qu'il y a un siècle ? Quels sont les avantages et les inconvénients ?
::::

Plus important pour nous, Herschel a écrit un livre, publié en 1830, intitulé *A Preliminary Discourse on the Study of Natural Philosophy*[^pd] (« discours préliminaire sur l'étude de la philosophie naturelle »). (« Philosophie naturelle » est un ancien nom de ce qu'on appellerait aujourd'hui « science ».) Cet ouvrage avait pour ambition d'exposer ce qu'il jugeait être de « bonnes méthodes » d'observation en science et d'élaboration de théories scientifiques. Il y donnait sa vision de ce que Newton entendait par « cause véritable »[^vc]. Une des utilités de la science, selon Herschel, est qu'elle nous fournit beaucoup de causes :

[^pd]: John F.W. Herschel, [*A Preliminary Discourse on the Study of Natural Philosophy*](https://doi.org/10.5962/bhl.title.19835), 1^e^ édition, Londres, 1830, éd. Longman, Rees, Orme, Brown & Green.
[^vc]: Si vous souhaitez approfondir sur ce débat, sachez que les scientifiques, historien(ne)s et philosophes qui en ont parlé, y compris Herschel, utilisent souvent l'expression latine originale du texte de Newton *vera causa*, à la place de « cause vraie » ou « cause véritable ».

:::: {.quote title="Herschel, \emph{Discours} (1830)"}
L'expérience ayant fait connaître la manière dont un phénomène dépend d'un autre dans une foule de cas, on se trouve pourvu d'une masse d'antécédents, de causes prochaines qui suivent les progrès de la science, et sont susceptibles lorsqu'elles sont diversement modifiées de produire une grande multitude d'effets, outre ceux qui originellement les ont fait connaître. C'est à ces causes que Newton a donné le nom de causes véritables.[^pd138]
::::

[^pd138]: Herschel, *Discours sur l'étude de la philosophie naturelle* (1830), édition française, traduction de B\*\*\*, Paris, 1834, éd. Paulin, p. 139-140.

Que veut dire Herschel ? Les découvertes scientifiques ont lieu dans des contextes très spécifiques. Par exemple, Newton a découvert la gravitation alors qu'il essayait de comprendre l'orbite des planètes et de la Lune ; au début des recherches sur l'électricité, de nombreuses théories ont été découvertes quand des gens ont [fait des expériences avec des batteries rudimentaires](https://fr.wikipedia.org/wiki/Bouteille_de_Leyde). Cependant, on s'aperçoit souvent que parce que ce sont des causes véritables, elles expliquent toutes sortes d'autres choses en plus : la théorie de Newton explique bien plus que l'orbite de la Lune et l'électricité a d'énormes applications en dehors des [farces et attrapes pour amuser ses amis en soirée.](https://www.atlasobscura.com/articles/shocking-scenes-from-benjamin-franklins-experimental-electricity-parties)

:::: think
**RÉFLEXION 5 :** Quand on propose une nouvelle théorie scientifique, on dispose d'un ensemble d'éléments déjà connus, qui nous ont suggéré la piste de cette théorie. Après avoir formulé la théorie, on découvre de nouveaux éléments de preuve, qui n'étaient pas connus avant qu'on avance la théorie. Beaucoup de philosophes et de scientifiques considèrent la différence entre preuves anciennes et preuves nouvelles comme très importante pour déterminer si une théorie est juste.

Quelles sont d'après vous les distinctions majeures entre preuve ancienne et preuve nouvelle ? Qu'est-ce qui justifierait de dire que les preuves nouvelles sont meilleures que les anciennes ou l'inverse ? Y a-t-il une différence entre le fait qu'une théorie « concorde » ou « soit compatible » avec des éléments anciens et le fait qu'elle « explique » de nouveaux éléments ?
::::

Ensuite, il y a le cas où on pense avoir découvert une nouvelle cause véritable. Imaginez que vous êtes Darwin. Vous pensez que la sélection naturelle est une nouvelle cause vraie. Que faites-vous ?

:::: {.quote title="Herschel, \emph{Discours} (1830)"}
Toutes les fois qu'on pense être arrivé par la voie de l'induction à la connaissance de la cause prochaine d'un phénomène \[\...\] la première chose à faire est d'examiner avec soin tous les cas de même espèce qu'on a rassemblés. Il faut s'assurer si effectivement la cause en rend bien compte.[^pd172]
::::

[^pd172]: Herschel, *Discours sur l'étude de la philosophie naturelle*, p. 162-163.

Dans sa langue un peu vieillie, Herschel nous dit ici qu'on doit se mettre en quête d'examiner chaque cas, dont on a connaissance dans le monde naturel, qui mette en jeu notre nouvelle cause, pour nous assurer qu'il peut s'expliquer de la façon qu'on croyait. Si on était Darwin, on devrait autant que possible se pencher sur tous les changements d'une espèce connus de nous et détailler comment ils pourraient avoir découlé de la sélection naturelle.

:::: think
**RÉFLEXION 6 :** Une partie importante de ce qui sépare Herschel et Darwin tient aux domaines respectifs dans lesquels ils travaillaient : Herschel était avant tout physicien et ingénieur et Darwin était un scientifique du vivant. Y a-t-il d'après vous des raisons pour que les niveaux d'exigence de la « science de qualité » en biologie doivent être différents de ceux de la physique ? Et en sciences sociales ? Est-ce que ces différences d'exigence signifient que certains domaines sont « plus scientifiques » que d'autres ?
::::

Et ce n'est pas fini. Comment savoir qu'on n'a pas simplement inventé une hypothèse très habile mais en fait fausse qui, par un complet **hasard**, se trouve expliquer les faits constatés jusqu'ici ? L'histoire des sciences est pleine [d'exemples de gens à qui c'est arrivé](https://fr.wikipedia.org/wiki/Phlogistique). Pour éviter ça, il faut poursuivre le travail :

:::: {.quote title="Herschel, \emph{Discours} (1830)"}
Mais une loi de la nature n'a pas ce degré de généralité qui la rend propre à servir de base à des inductions plus générales, si elle n'est pas **universelle** dans son application \[\...\] La première chose à faire quand on veut vérifier une induction est donc de chercher à l'étendre à des cas que, dans le principe, on n'avait pas en vue, à varier les circonstances dans lesquelles les causes agissent, afin de s'assurer si leur action est générale, et à pousser l'application de nos lois aux cas extrêmes.[^pd176]
::::

[^pd176]: Herschel, *Discours sur l'étude de la philosophie naturelle*, p. 165-166.

Il faut aller chercher des cas nouveaux, c'est-à-dire des faits dont on n'avait même pas connaissance quand on a rédigé la théorie, et s'assurer qu'ils ne posent pas non plus problème. Il faudra inclure des exemples similaires à ceux déjà connus mais aux conditions légèrement différentes, ainsi que des cas extrêmes, bizarres, des situations susceptibles de faire s'écrouler notre nouvelle cause, et s'assurer que là aussi, notre cause fonctionne.

On commence à voir clairement pourquoi Herschel ne pouvait pas approuver la science de Darwin. Il était tout simplement impossible, vu les connaissances en biologie de l'époque (et même celles d'aujourd'hui) d'examiner **chaque changement** survenu au cours de l'histoire de la vie et de réfléchir à une description en termes de sélection naturelle. Et il n'y avait pas de moyen à l'époque de Darwin de **produire délibérément** de nouveaux cas de sélection naturelle pour vérifier que la théorie fonctionnait correctement. (C'est quelque chose dont nous sommes aujourd'hui capables, par exemple dans la grande [Expérience d'évolution à long terme ou LTEE)](https://en.wikipedia.org/wiki/E._coli_long-term_evolution_experiment).

:::: think
**RÉFLEXION 7 :** Une autre distinction qui éclaire la différence entre les conceptions de la science de Herschel et de Darwin est de considérer séparément les sciences qui procèdent surtout par expérimentation contrôlée et les sciences qui font principalement appel à l'observation des phénomènes dans le monde qui nous entoure. Quelles différences dans les données ou dans la théorie pourraient en découler ? Quels protocoles scientifiques sont possibles dans un cas mais pas dans l'autre ?

On décrit parfois une thèse voisine de philosophie des sciences en parlant de l'importance « d'apprendre en faisant » ou « apprendre en fabriquant ». Est-ce qu'on en sait plus sur un système scientifique quand on est capable d'en créer un soi-même ? Ou est-ce qu'on peut acquérir toutes les informations utiles en observant des systèmes « dans la nature » ?
::::

Herschel a d'ailleurs tout fait pour que Darwin sache qu'il n'aimait pas sa théorie. Malgré le profond respect que lui portait Darwin et bien que *L'origine des espèces* semble suivre le modèle prescrit par Herschel, ce dernier a inclus dans un livre qu'il a publié quelques années après *L'origine* de Darwin une longue note de bas de page expliquant ce qu'il lui reprochait :

:::: {.quote title="Herschel, \emph{Géographie physique} (1861)"}
\[\...\] on ne peut pas plus accepter le principe d'une variation arbitraire et aléatoire et de la sélection naturelle comme explication suffisante *per se* du monde organique passé et présent qu'on ne peut accepter la méthode de composition des livres à Laputa (poussée à son comble) comme suffisant à expliquer Shakespeare et les *Principes* \[de Newton\].[^geognote]
::::

[^geognote]: John F.W. Herschel, [*Physical Geography: From the Encyclopaedia Britannica*](https://archive.org/details/physicalgeograph00hers), Édimbourg, 1861, éd. Adam and Charles Black, section 11, note. Traduction de l'anglais : Sandra Mouton.

Si vous ne connaissez pas *Les voyages de Gulliver*, le héros de ce livre visite le pays de l'île de Laputa, où on génère des livres grâce à une machine qui combine des séries aléatoires de mots avant de lire le résultat pour voir s'il contient quoi que ce soit de compréhensible. Pour Herschel, la théorie de l'évolution de Darwin ressemble à ça. Herschel continue :

:::: {.quote title="Herschel, \emph{Géographie physique} (1861)"}
Dans un cas comme dans l'autre, une intelligence, guidée par une fin, doit être continuellement *à l'œuvre* pour influer sur l'orientation des étapes de changement, pour réguler leur intensité, pour limiter leurs divergences et pour les continuer selon une voie définie. Nous ne croyons pas que M. Darwin veuille nier la nécessité d'une telle direction intelligente. Mais, pour autant que nous puissions le constater, \[cette direction intelligente\] n'entre pas dans l'équation de sa loi et, sans elle, nous ne pouvons concevoir comment cette loi peut avoir conduit à ces résultats.
::::

Deux choses, si elles étaient ajoutées par Darwin à sa théorie, pourraient la rendre acceptable aux yeux de Herschel. L'une d'elles serait la « direction intelligente », c'est-à-dire l'intervention divine, qui modifierait le cours du changement évolutif car elle pourrait voir exactement quels changements seraient bénéfiques pour un organisme et lesquels seraient problématiques.

:::: {.quote title="Herschel, \emph{Géographie physique} (1861)"}
En revanche, nous n'entendons pas disputer que cette intelligence puisse agir conformément à une loi (c'est-à-dire selon un plan prédéfini et réglé). Une telle loi, exprimée en mots, ne serait autre que la loi effectivement observée de succession des organismes, ou une loi plus générale qui, appliquée à notre planète, prendrait cette forme et inclurait tous les chaînons ayant disparu. Mais cette loi est un complément indispensable de l'autre et doit, en bonne logique, faire partie de son énoncé. Moyennant ce point et avec quelques réserves concernant la création de l'homme, nous ne sommes nullement enclin à rejeter l'opinion sur ce mystérieux sujet adoptée dans l'ouvrage de M. Darwin.
::::

L'autre ajout que Darwin pourrait faire serait une « loi de variation » selon laquelle fonctionnerait l'évolution, à peu près de la même manière que les planètes orbitent selon la loi de la gravitation. (Cette loi, selon Herschel, avait été fixée par intervention divine.) Mais cette loi de variation exigerait précisément ce que Darwin ne pouvait pas faire, comme nous l'avons vu plus haut, « la loi effectivement observée de succession des organismes » ou, pour dire plus simplement, les étapes précises de variation subies par le monde naturel. Darwin ne pouvait bien sûr pas les connaître, nous ne le pouvons pas non plus, et donc Herschel rejette entièrement l'évolution.

:::: think
**RÉFLEXION 8 :** Comment répondriez-vous à ces critiques, si vous étiez Darwin ? Doit-il modifier sa théorie pour prendre en compte le point de vue de Herschel ?
::::

Avant d'approfondir l'analyse des critiques de Herschel pour comprendre où il a fait erreur, lisons une source supplémentaire. Il se trouve qu'il est possible de consulter dans une bibliothèque du Texas l'exemplaire de *L'origine des espèces* que possédait Herschel. Il contient les notes manuscrites prises par celui-ci pendant sa lecture.[^marginalia]

[^marginalia]: Ces notes marginales sont dans le domaine public et peuvent être consultées en ligne : <https://doi.org/10.5281/zenodo.3974296>

Au chapitre 5 de *L'origine*, Darwin aborde la cause des variations, c'est-à-dire pourquoi les descendants ne ressemblent pas exactement à leurs parents. Cette variation est cruciale pour la théorie de l'évolution puisqu'on n'assistera pas à la production de nouvelles adaptations s'il ne se produit pas les bonnes variations. Mais Darwin n'a pas notre connaissance de la génétique, encore moins de l'ADN, il ne sait donc pas comment les variations se produisent. Il pouvait seulement dire qu'il semblait avéré qu'elles étaient **effectivement** générées par quelque chose (comme le prouvait par exemple les succès rencontrés en agriculture).

Pourtant, Herschel n'était toujours pas satisfait. Après ce chapitre de *L'origine*, Herschel a écrit dans son exemplaire :

:::: {.quote title="L'exemplaire de \emph{L'origine} (1859) de Herschel"}
D. reconnaît une cause inconnue aux légères différences individuelles mais affirme que la « sélection naturelle » a le caractère de « théorie suffisante » en ce qui concerne les résultats de ces différences[^margtrans].
::::

[^margtrans]: Traduction de l'anglais : Sandra Mouton.

Sur la même page, Herschel a souligné deux passages du texte de Darwin dans le dernier paragraphe du chapitre, comme pour les opposer l'un à l'autre. D'abord, il souligne la phrase où Darwin dit « chacune \[des variations\] doit avoir sa cause » puis il souligne deux fois l'affirmation « \[la sélection naturelle\] **donne essor** à toutes les modifications de structure plus importantes[^origtrans] ».

[^origtrans]: La version française de ces deux citations de *L'origine des espèces* de Darwin est tirée de la traduction française par Thierry Hoquet, Paris, 2013, éd. du Seuil.

Herschel semble demander ce qui est réellement derrière l'apparence d'adaptation, ce qui est important pour créer de nouvelles espèces : les causes des variations ou la sélection naturelle ? Et si la réponse est « les deux », comment une théorie de la sélection naturelle peut-elle fonctionner sans théorie de la variation pour l'accompagner ?

:::: think
**RÉFLEXION 9 :** Nous pourrions reformuler de la manière suivante cet aspect du désaccord qui oppose Herschel à Darwin. Darwin pense que, parce qu'il n'existe pas à son époque de théorie décrivant comment fonctionne la variation dans la nature, il peut aborder l'apparition des variations comme un « système de boîte noire ». Pour lui, nous avons assez d'éléments de preuve pour conclure que la boîte noire fonctionne, même si nous ne savons pas comment. Herschel, en revanche, pense que nous devons ouvrir la boîte noire et fournir une explication de ce qui s'y passe, une explication de la façon dont sont créées les variations dans le monde réel.

Voyez-vous une résolution possible à ce désaccord entre eux ? Plus généralement, en présence d'un débat entre scientifiques non pas sur des constatations empiriques mais sur les exigences à appliquer aux explications scientifiques, vers quelles ressources devons-nous nous tourner pour trancher ? Qu'est-ce qui à vos yeux serait un argument valable de la part de Herschel ou de Darwin en faveur de leurs conceptions respectives ? Qui devrait décider des normes et exigences s'appliquant aux explications ?
::::

![John Herschel, portrait par la célèbre photographe Julia Margaret Cameron (image : domaine public, Wikimedia Commons)](herschel.jpg){.smallimg width=33%}

Changeons de perspective et penchons-nous sur la réaction générale de Herschel à la théorie de Darwin. Pour Herschel, il y a deux modes de fonctionnement possibles des changements dans le monde naturel, soit ils sont dirigés par une entité divine soit ils fonctionnent selon les lois de la nature, qui génèrent ses « causes véritables ». La sélection naturelle **pourrait**, à son avis, être une cause véritable. Mais pour avancer cet argument, il nous faudrait démontrer deux choses qui étaient hors de la portée de Darwin en 1859. Primo, il nous faudrait montrer que de nombreux changements survenus dans une espèce peuvent s'expliquer par la sélection naturelle. Et secundo, il nous faudrait comprendre la loi de variation, pour qu'il soit certain que la variation nécessaire à la production du nouvel organisme serait opérante au bon moment au bon endroit.

Nous pouvons utiliser les analyses de Herschel dans un certain nombre de réflexions importantes concernant l'évolution. Tout d'abord, Herschel a tout à fait raison, il manque **effectivement** quelque chose à la théorie de Darwin. Pendant le demi-siècle qui a suivi la publication par Darwin de *L'origine des espèces*, les scientifiques ont eu du mal à comprendre ce qu'était la variation, comment elle fonctionnait et comment la concilier avec la structure du vivant et les changements évolutifs. Avant qu'on ne commence à comprendre les fondements génétiques de l'évolution, de 1859 jusqu'aux années 1920, les biologistes ont travaillé sur divers moyens de contourner le problème, depuis des théories de l'évolution qui ne mentionnaient pas vraiment comment les descendants héritaient des caractères de leurs parents jusqu'à des théories spéculatives sur des modes de fonctionnement possibles de la variation, basées sur les connaissances limitées en biologie cellulaire de l'époque. Même si Herschel avait tort de considérer que la biologie devait être gouvernée par une « loi de variation », il avait discerné que notre compréhension de l'évolution serait bien plus puissante si nous parvenions à la raccorder à une connaissance des causes des variations et des moyens de leur transmission héréditaire.

\enlargethispage{-\baselineskip}

:::: think
**RÉFLEXION 10 :** Qu'est-ce que nous obtiendrions de plus en injectant de la génétique dans l'évolution ? Une réponse possible est que nous comprendrions ce qui se passe à un niveau « inférieur », plus fondamental (à savoir la biochimie) et que ce serait le moyen de comprendre ce qui se passe au « niveau supérieur » (à savoir les organismes). La tendance est marquée en faveur de ce type de **réductionnisme** en science, l'idée selon laquelle les explications opérant à un niveau plus bas, plus fondamental, sont meilleures que celles des niveaux au-dessus.

Pensez-vous que ce type de réductionnisme est valide ? Quels sont les avantages et les inconvénients d'une explication en termes de chimie, par rapport à une explication biologique ? Quelles conséquences cela peut-il avoir sur notre vision des relations entre différents domaines scientifiques ?
::::

![Deux phalènes du bouleau, une de la forme claire et une de la forme sombre (image : CC-BY-SA, Siga, Wikimedia Commons)](moths.jpg){.smallimg .left width=33%}

Le second point sur lequel Herschel a raison est qu'il est difficile --- parfois très difficile --- de retracer l'histoire possible de l'apparition d'une caractéristique spécifique par sélection naturelle. Quand on a de la chance, c'est plus simple, comme dans le fameux exemple des [phalènes du bouleau dont la couleur a changé quand la pollution a assombri la couleur des arbres sur lesquelles elles vivaient](https://doi.org/10.1038/hdy.2012.92). Cependant, c'est souvent plus compliqué et il faut effectivement procéder avec prudence. On peut parfois obtenir beaucoup plus d'information détaillée, en particulier de nos jours, quand on peut [rattacher un scénario évolutif et écologique à une base moléculaire et biochimique](https://www.youtube.com/watch?v=5UkxNkuc_OY), ce qui était bien évidemment impossible pour Darwin.

Enfin, il est vrai que la théorie de l'évolution ne fonctionne pas de la manière dont Herschel pensait qu'une théorie scientifique devait être. Elle exige de nous que nous utilisions des types d'arguments différents à l'appui de notre position. Mais contrairement à Herschel, rien ne nous oblige à dire que ces arguments différents sont automatiquement inférieurs. L'inférence à la meilleure explication, par exemple, est un type d'argument dont nous faisons usage quotidiennement dans la vie. Pensez à une intrigue policière classique. L'enquêteur/trice imagine toutes les façons possibles dont le meurtre peut avoir été commis puis part à la recherche de toutes les preuves possibles, en espérant qu'elles indiqueront qu'une de ces explications est clairement meilleure que les autres. Bien sûr, la personne chargée de l'enquête ne se met pas à faire des expériences et ne collecte pas des données comme Herschel l'aurait voulu. Mais quand elle formule une théorie sur le/la coupable, elle a souvent raison !

:::: think
**RÉFLEXION 11 :** La différenciation entre **types** d'arguments est une préoccupation ancienne dans l'étude de la philosophie. Certains raisonnements, comme en mathématique ou en logique, sont **déductifs** : si les prémisses sont vraies, les conclusions sont **nécessairement** vraies. D'autres, comme dans l'approche de la science de Herschel, sont **inductifs** : on passe d'un vaste ensemble de données à une théorie générale sur ces données. Enfin, d'autres sont **abductifs** : on adopte une explication des phénomènes qui nous intéressent parce qu'elle est beaucoup plus convaincante que les autres possibilités.

Avez-vous des exemples d'arguments scientifiques qui utilisent chacun de ces trois modes de raisonnement ? Certains vous semblent-ils plus puissants que d'autres ? Les différents types peuvent être disponibles ou non dans différents domaines à différents moments, selon le type de preuves que nous avons. Est-ce que cela fonde un jugement sur la « qualité » globale de ces domaines scientifiques ?
::::

Pour résumer, les types d'arguments en science, surtout quand on laisse de côté la physique et qu'on s'intéresse à la science biologique ou aux sciences sociales, sont extrêmement variés et il n'est pas facile de comprendre les liens qui les relient tous entre eux. La science de l'évolution est complexe et difficile et on devrait se montrer sceptique face à quiconque nous dit que toutes les recherches scientifiques doivent suivre une méthodologie unique.


## RÉFLEXION : questions sur la nature des sciences

Qu'est-ce que le désaccord entre Herschel et Darwin nous dit sur les aspects suivants des sciences ?

* complétude de la preuve
* robustesse (concordance entre différents types de données)
* pertinence des preuves (empirisme)
* consilience vis-à-vis de preuves avérées
* rôle de l'analogie, réflexion interdisciplinaire
* formes de persuasion
* réponse aux critiques


## Lectures d'approfondissement

* James A. Secord, « The Conduct of Everyday Life: John Herschel's Preliminary Discourse on the Study of Natural Philosophy », [*Visions of Science: Books and Readers at the Dawn of the Victorian Age*](https://www.worldcat.org/title/visions-of-science-books-and-readers-at-the-dawn-of-the-victorian-age/oclc/923634059), Chicago, 2014, éd. University of Chicago Press, p. 80-106.
* W.F. Cannon, « John Herschel and the Idea of Science », *Journal of the History of Ideas*, 1961, vol. 22, n^o^ 2, p. 215-239, <https://doi.org/10.2307/2707834>.
* M.J.S. Hodge, « Darwin's Argument in the Origin », *Philosophy of Science*, 1992, vol. 59, n^o^ 3, p. 461-464, <https://doi.org/10.1086/289682>.

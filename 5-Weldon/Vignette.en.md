# 5. W. F. R. Weldon, Genes, and Traits

![Gregor Mendel, from Bateson (1902; public domain; Wikimedia Commons)](mendel.jpg){.smallimg .left width=33% footnote="Bateson, William. 1902. _Mendel's Principles of Heredity: A Defence: With a Translation of Mendel's Original Papers on Hybridisation._ Cambridge: Cambridge University Press. <https://doi.org/10.5962/bhl.title.95694>."}

As you probably all know by now, a monk named [Gregor Mendel](https://en.wikipedia.org/wiki/Gregor_Mendel) (1822--1884) performed a series of experiments in the middle of the nineteenth century [on a variety of characters in peas](https://www.ncbi.nlm.nih.gov/books/NBK22098/) -- experiments that would later become central in the development of the science that we now call genetics. Mendel showed that for a number of the traits of these peas, like their color (yellow or green), whether they were smooth or wrinkled, and whether they were round or irregularly shaped, these traits are passed from parents to offspring in a very peculiar way. In our modern terminology, each parent seems to carry two alleles at each locus, two "versions" of the character, passing one (selected at random) on to each child. Moreover, the connection between those two alleles and the way that the peas actually looked was exceptionally clear. Some traits, like being yellow instead of green or being smooth instead of wrinkled, are what he called "dominant" -- if a pea has just one copy of yellow, it will appear yellow. The other trait in each pair is what he called "recessive" -- a pea needs *two* copies of the green allele to appear green. Mendel thus described this as the yellow color being dominant over the green in cases when they're mixed.

:::: think
**THINK[1]:** What do you already know about Mendel and his work? How was he presented in your textbook?
::::

![A pod from the _Pisum sativum_ plant (CC-BY-SA; by Rasbak at Wikimedia Commons)](pisum.jpg){.smallimg width=33%}

So much for the common history and biology. What your book might not have told you was this: when Mendel published his paper in 1865, almost nobody cared. It wasn't until 1900, when three biologists named [Hugo De Vries](https://en.wikipedia.org/wiki/Hugo_de_Vries) (1848--1935), [Carl Correns](https://en.wikipedia.org/wiki/Carl_Correns) (1864--1933), and [Erich von Tschermak](https://en.wikipedia.org/wiki/Erich_von_Tschermak) (1871--1962) were all independently looking for [evidence of "jumps" in characters,](https://en.wikipedia.org/wiki/Mutationism) just like that from yellow to green or smooth to wrinkled, that all three found Mendel's paper. It was a smash hit. Ever since Darwin, as we have seen in several of our previous readings, biologists had been worried that we didn't really have a theory that could explain how organisms varied, and how those variations were passed from parents to offspring. But if we didn't have that, how could we ever really know how natural selection worked? Would we ever be able to really understand evolution?

Some biologists thought that Mendel's paper would provide a nice, easy answer to this question. Every character of every organism, from the yellowness of a pea to the intelligence of a person, would simply be governed by a small number of genes. If we could understand those genes, we could control them -- learning about the ["gene for tasty tomatoes"](https://www.smithsonianmag.com/smart-news/tastier-tomatoes-may-be-making-comeback-180972175/) would let us breed the world's best tomatoes.

As we now know, however, genetics is *much* more complicated than this. There is a *massive* number of genes involved with almost any trait, and those genes are intimately connected to the environment in which an organism grows up. Many genes engage in extremely complicated interconnections, where networks of different genes are responsible for "regulating" the products of other parts of the network, with some genes making it possible for others to operate, and so on.

:::: think
**THINK[2]:** It is important to remember that, when Mendel's work was first published and evaluated, we had not yet discovered the fundamentals of cellular biology, biochemistry, or DNA. What kind of data would you be able to accumulate, then, and how could you use it to fashion scientific theory? What would have been the limits of the kind of theories you could construct on this basis?
::::

![W. F. R. Weldon, from Pearson (1906; public domain; scanned by Pence)](weldon.jpg){.smallimg .left width=33% footnote="Pearson, Karl. 1906. \"Walter Frank Raphael Weldon. 1860--1906.\" _Biometrika_ 5 (1/2): 1--52. <https://doi.org/10.1093/biomet/5.1-2.1>."}

The reading we'll look at today is one of the early chapters in this discovery of the complexity of genetics, and the goal is to think about one of the fundamental concepts at work in Mendel's very first experiments: *what is a gene?* How did we decide what a single "trait" is? How could you make that choice in a world where you didn't know about biochemistry or DNA, and were just looking at organisms in the world around you? And what's the relationship between Mendel's way of looking at genetics -- where we observe the genetic makeup of two individual parents and the offspring that they might have -- and an evolutionary perspective, where it is *populations* of organisms that matter?

As Mendel's paper became increasingly famous, more and more biologists started to take notice. One of them was [W. F. R. Weldon](https://en.wikipedia.org/wiki/Raphael_Weldon) (he went by Raphael; 1860--1906), a young invertebrate zoologist who worked in London. He had spent the last few years developing, with several colleagues, [a new approach to the understanding of evolutionary populations,](https://www.britannica.com/science/probability/Biometry) and was one of the first people who had ever used mathematics, especially statistics, to think in a serious way about evolution.

:::: think
**THINK[3]:** Darwin had introduced his entire theory of evolution without, essentially, any math at all. How do you think the addition of mathematical methods to biology could have been seen by some biologists as a welcome addition to biological practice, and by others as a way in which to add all kinds of unnecessary complexity and a bad idea?
::::

At first, when he read Mendel's paper, he was excited -- he wrote to another scientist that he thought Mendel's data could be a perfect way for us to understand more about characters like human eye color, where it seems like there are only a few available "types" in nature (blue eyes, green eyes, brown eyes, etc.), and the types don't seem to blend together very often. But as he started to look at peas more closely, he got worried. He wrote out the details in a journal article that was published in 1902:

:::: {.quote title="Weldon, from the journal \emph{Biometrika} (1902)"}
Mendel's statements are based upon work extending over eight years. The remarkable results obtained are well worth even the great amount of labour they must have cost, and the question at once arises, how far the laws deduced from them are of general application. It is almost a matter of common knowledge that they do not hold for all characters, even in Peas, and Mendel does not suggest that they do. At the same time I see no escape from the conclusion that they do not hold universally for the characters of Peas which Mendel so carefully describes. In trying to summarize the evidence on which my opinion rests, I have no wish to belittle the importance of Mendel's achievement. I wish simply to call attention to a series of facts which seem to me to suggest fruitful lines of inquiry.[^weldon1]
::::

[^weldon1]: Weldon, W. F. R. 1902. "Mendel's Laws of Alternative Inheritance in Peas." _Biometrika_ 1 (2): 228--254, p. 235. <https://doi.org/10.1093/biomet/1.2.228>.

![W. F. R. Weldon's image of the characters in different strains of peas (public domain; from Weldon [1902], scanned by Pence)](weldon_peas.jpg){.lgimg width=100%}

It's important to say that Mendel was very careful in his paper. He only talks about seven characters of one plant, and he makes very clear that he does not mean to claim that every character would behave in exactly the same way. To take a simple example of a trait that would work in a different way, human height is extremely complex, because it involves the interaction of so many factors -- muscles, bones, tendons, and so on -- and so there's no way that you could think about someone's height being the result of some particular single gene. Mendel would have agreed completely, and this wasn't controversial.

What was controversial was Weldon's claim that even in peas, and *even for some of the characters Mendel studied,* Mendel's explanation couldn't work. The image you see above is a photograph that Weldon had prepared of a variety of different peas that he harvested himself, after sending letters all over England in search of different kinds of peas from agricultural seed suppliers. What he found was surprising.

:::: think
**THINK[4]:** Including this color photograph in his journal article was both difficult and expensive at the time. Weldon was thus clearly attempting to persuade his readers in a particular kind of way. How might a photograph have been more persuasive than just a description? What might Weldon have been trying to show his readers about himself or his scientific process by including it?
::::

Let's just think about color for a moment. Remember that on Mendel's explanation, peas are either green or yellow, depending on whether they have the green or yellow alleles, and yellow is dominant while green is recessive. But look at the first row of pictures, numbered 1--6. Weldon has found -- from a single variety of peas -- examples that seem to move very smoothly from a pure green (number 1) to a sort of yellow-orange (number 6). For that matter, he even finds peas that don't even seem to be the same color all over (numbers 13--18).

The same trouble occurs for all of the traits that Mendel measured. As he described it:

\enlargethispage{\baselineskip}

:::: {.quote title="Weldon, from the journal \emph{Biometrika} (1902)"}
Many races[^race] of Peas are exceedingly variable, both in colour and in shape. A race with "round smooth" seeds, for example, does not produce seeds which are exactly alike; on the contrary, many seeds...show very considerable irregularities; while in [others,] hardly any two seeds are alike. So that both the category "round and smooth" and the category "wrinkled and irregular" include a considerable range of varieties. At the same time, the categories are undoubtedly very often discontinuous, the most wrinkled seed of [some peas] being so much smoother and more rounded than the most regular seed of the typically "wrinkled" races, that no one who knows both races would hesitate for a moment in deciding which race a given seed resembled.[^weldon2]
::::

[^race]: Weldon is using the word "race" as an antiquated term for what we would call a "variety" or "strain" of peas; there is only a very loose connection with the idea of "races" in humans. For more information, see Tanghe, Koen B. 2019. "On _The Origin of Species_: The Story of Darwin's Title." _Notes and Records_ 73 (1): 83--100. <https://doi.org/10.1098/rsnr.2018.0015>.

[^weldon2]: Weldon, p. 236.

He identifies two more problems here. First, the peas that Weldon has been studying are often extremely *variable.* A population of only "round"-seeded peas will have seeds that are quite different from one another. The trait "round," it seems, can be expressed in many different ways. The second problem is that what "round" or "smooth" means might be different from one kind of peas to another. If you had spent a long time observing one variety of pea, such that you were pretty sure you knew what "smooth" meant, and then you started observing a different variety, you might find that even the most wrinkled pea of your new variety would have been called "smooth" before!

In short, Weldon's first major critique of Mendel is that the categories that he draws simply don't correspond to biological reality. We will have to allow for more complex ways of describing the world if we want to understand the full diversity of even something as simple as the colors of peas.

:::: think
**THINK[5]:** If the data here are as uncertain and variable as Weldon says they are, we have an interesting challenge that we have to solve: how do we do science, in spite of these problems with our data? How can we avoid just interpreting the data we have in terms of our prior beliefs (confirmation bias)? What kinds of techniques can you think of that could help us overcome this problem?
::::

![Some "fancy" pigeons (public domain; illustration by Anton Schöner, Wikimedia Commons)](pigeons.jpg){.medimg width=66% footnote="Schachtzabel, Emil. 1906. _Illustriertes Prachtwerk sämtlicher Taubenrassen._ Würzburg: Königl. Universitätsdruckerei H. Stürtz A. G."}

His second worry is related to something that evolutionary biologists had been talking about extensively since it made an important appearance in Darwin's work. Biologists at this time had collected a lot of data on a very odd phenomenon. Sometimes, when people breed a very peculiar kind of plant or animal (like these "fancy" pigeons), even if they keep breeding them for a very long time, suddenly there will be [what they called a "reversion" or "atavism"](https://www.sciencedirect.com/topics/biochemistry-genetics-and-molecular-biology/atavism) -- a case where the animal suddenly snaps back to being like a very distant ancestor (like a pigeon that you'd see in the street).

:::: think
**THINK[6]:** What kinds of possible explanations can you think of for how this phenomenon might happen?
::::

Not only is that really strange, but it seemed to Weldon to mean that we have to think about the impact that *distant ancestors* have on current organisms. And *that* doesn't work with Mendel's theory. For Mendel, the only thing that matters is the genes that your parents have -- we don't need to think about anything farther back in order to understand what genes you'll have:

:::: {.quote title="Weldon, from the journal \emph{Biometrika} (1902)"}
Mendel treats such characters as yellowness of cotyledons and the like as if the condition of the character in two given parents determined its condition in all their subsequent offspring. Now it is well known to all breeders...that the condition of an animal does not as a rule depend upon the condition of any one pair of ancestors alone, but in varying degrees upon the condition of all its ancestors in every past generation, the condition in each of the half-dozen nearest generations having a quite sensible effect.[^weldon3]
::::

[^weldon3]: Weldon, p. 241.

From today's perspective, we can understand better what might cause these kinds of effects -- the kinds of gene networks that we briefly talked about above, where [a massive number of genes all interact with each other, with complex relationships of regulation and control,](https://en.wikipedia.org/wiki/Gene_regulatory_network) could produce exactly these kinds of outcomes, where genes that had been "switched off" for a number of generations were suddenly switched back on. But that's only an answer available to us today; neither Mendel nor Weldon ever imagined that things would be that complicated. What Weldon was sure of, though, was that there had to be more going on than Mendel acknowledged.

:::: {.quote title="Weldon, from the journal \emph{Biometrika} (1902)"}
These examples, chosen from many others which might have been cited, seem to me to show that it is not possible to regard dominance as a property of any character, from a simple knowledge of its presence in one of two individual parents. The degree to which a parental character affects offspring depends not only upon its development in the individual parent, but on its degree of development in the ancestors of that parent.[^weldon4]
::::

[^weldon4]: Weldon, p. 244.

Weldon's conclusion on the basis of reversion, then, is that dominance cannot be as simple as Mendel described it. If whether or not a character in an offspring is expressed involves more than whether or not that character was present or absent in the parents, then there is something else, more than just dominance, controlling the expression of characters. That means that dominance is more than a simple property of characters -- we have to tell a genetic story more complicated than just saying that yellow is dominant and green is recessive. If that's true, "being dominant" isn't a thing that's a simple fact about the yellow allele.

If the connection between what genes an organism has and what kinds of characters it displays isn't only a question of dominance, how does Weldon think we should understand it instead? As mentioned above, Weldon was one of the first biologists to seriously argue for the use of statistics in evolutionary theory. If we want to understand characters like the height of people, we know that we won't be able to think about one gene for "tall" or "short." What we need to do is to consider the [curve that describes the heights of all people](https://en.wikipedia.org/wiki/Normal_distribution) -- lots of medium-sized people in the middle, and a few really tall and really short people on the ends.[^sex]

[^sex]: To really understand human height, there's a lot more that we have to do, too: we also need to account for the fact that men are usually taller than women, that people who eat better as children are taller than people who don't, and many other factors.

:::: think
**THINK[7]:** If you believed that most characters were actually transmitted like those in Mendel's peas, what other kind of explanation could you give for those characters like height, which seem to be distributed in a very different way? What sorts of evidence would we need to decide between Weldon's explanation and yours?
::::

Weldon, then, argues that Mendel has made a mistake precisely because he doesn't treat characters as these kinds of statistical curves. He wrote a letter to a colleague in 1902 that described his worry as follows:

:::: {.quote title="Weldon, letter to Pearson (1902)"}
What all Mendelians do, is to take the diagram of frequency

![](graph.png){.lgimg width=100%}

and to call a range AB one "character," and the range BC the other "character" of a Mendelian pair.

Now if you take a parent from anywhere on the A side of the mean value, and mate it with a parent out of the range BC, with any family of experimental size the chance that offspring will occur which fall within the range BC must be nearly = 0.

That at once gives "imperfect dominance" of AB over BC.[^letter]
::::

[^letter]: This letter, written on June 23, 1902, is found in the [Karl Pearson archives at University College, London,](https://archives.ucl.ac.uk/CalmView/Record.aspx?src=CalmView.Catalog&id=PEARSON) in box PEARSON/11/1/22/40.7.3.

Look at the graph that he drew, and imagine that it's describing the color of peas, like we saw across the top of the photograph above. Weldon argues that this character should be seen as a curve -- just like height in people. If we want, perhaps we could say that "green" or "yellow" were clearly defined characters, which would mean dividing up the curve and separating the very extreme green peas from the less extreme, yellower peas. We could do the same thing, Weldon argues, with continuously distributed characters like height. Think about the children of a very tall mother and a very short father -- they might, if the statistical relationships between the parents came out right, produce something that looked like Mendelian inheritance. But the reality underlying that appearance would actually be much, much more complicated. Weldon was attempting to argue that exactly the same thing was happening in the case of the peas. We might simplify in certain kinds of ways and get Mendelian ratios. But these would be, in some sense, nothing more than useful theoretical tools, not reflections of the way the world really is.

:::: think
**THINK[8]:** One of the questions at play in Weldon's response has to do with the purpose of scientific theory. Why do we build scientific theories in the first place? Are we trying to use them just to make predictions about the world around us, or are our scientific theories supposed to tell us what the world is made out of and how those parts work together? Do you think one of those gives us "better" science? Which one?
::::

Of course, we know today that in some sense, this was exactly right: the underlying reality was even more complex than Weldon (or anyone else) had realized at the time. What kinds of lessons can we take from Weldon's caution for contemporary biology?

Most importantly, we should be careful not to read too much into Mendel's peas. We now know that, actually, Mendel was correct. The characters that he was studying really were the result of changes in a single gene, and really did have pretty close to complete dominance in the case of the pea plants that he studied. But this is, as Weldon sensed, [an extremely unusual case in nature;](https://en.wikipedia.org/wiki/Dominance_(genetics)) very few traits work like this. That means that we have to be very careful whenever we hear stories about [a "gene for" a particular character.](https://dx.doi.org/10.1002%2Fbies.200800133) Very often, especially in the media, a story about a statistical connection between a gene and an outcome that's really important to everyday people will be reported as the "discovery of the gene for" some kind of disease or mental illness. It is exactly this kind of jump from genes to complicated outcomes that Weldon wanted to warn us against.

:::: think
**THINK[9]:** Have you ever seen examples like this from the media in your own experience? For instance, say that someone claimed to have found the "gene for obesity?" How might you critically evaluate that kind of a claim? What sorts of further evidence do you think we'd need to know whether or not it was really true?
::::

An additional factor that Weldon's critique can draw our attention to relates to another way in which actual outcomes for organisms are made much more complex than simple Mendelian patterns might lead us to think: [the relationship between genes and the environment.](https://en.wikipedia.org/wiki/Gene%E2%80%93environment_interaction) Genes, of course, don't do anything by themselves. A real characteristic of an organism results from a gene expressed over the lifetime of an individual, which lives, eats, and interacts in an environment that includes other organisms, natural resources, food, members of other species, and countless other influences. All of these interactions matter for the future lives of organisms -- not only genes.

:::: think
**THINK[10]:** Imagine that we had two competing explanations for a single phenomenon -- one genetic and one environmental. How might we compare and contrast them? What would you want to know to be able to choose between them?
::::

We can see a bit of the importance of this within Weldon's critique itself. While we don't have time to talk about the full history of the idea of "reversion" here, another reason that the reappearance of characters from distant ancestors might occur in the wild is because these environmental impacts on organisms might repeat themselves -- being raised in a particular kind of environment, just as an ancestor was, might cause the expression of traits that hadn't been seen since that ancestor lived.

In short, since the very birth of genetics there has been a tension between looking for simplistic genetic explanations of complex phenomena -- which occasionally can be found! -- and cautionary approaches to the role of genes and their relationships both with one another and with the environment. Especially when important questions of [the food supply](https://www.who.int/foodsafety/areas_work/food-technology/faq-genetically-modified-food/en/) or [human health](https://www.genome.gov/For-Patients-and-Families/Genetic-Disorders) are on the line, it's important for us to think carefully about these consequences!


## THINK: NOS Reflection Questions

What does the story of Weldon's response to Mendel tell us about the following features of the nature of science?

* robustness (agreement among many different types of data)
* confirmation bias
* interdisciplinary thinking
* new techniques and their validation
* forms of persuasion
* error and uncertainty
* credibility of news media


## Further Reading

* Pearson, Karl. 1906. "Walter Frank Raphael Weldon. 1860--1906." _Biometrika_ 5 (1/2): 1--52. <https://doi.org/10.1093/biomet/5.1-2.1>.
* Radick, Gregory. 2005. "Other Histories, Other Biologies." _Royal Institute of Philosophy Supplement_ 56 (3--4): 21--47. <https://doi.org/10.1017/S135824610505602X>.
* Radick, Gregory. 2016. "Teach Students the Biology of Their Time." _Nature_ 533 (7603): 293. <https://doi.org/10.1038/533293a>.
